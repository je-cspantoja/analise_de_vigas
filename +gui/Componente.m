classdef (Abstract)Componente<gui.Grafico
    
   
    properties(Constant)
    
    centro='center';
    esquerda='left';
    direita='right';
    
    end
    
    events
    componenteClicado;
    end
    
    methods
        
        function obj=Componente(id)
            obj@gui.Grafico(id);
            
            tipo=get(id,'Type');
            
            if strcmp(tipo,'uicontrol')
            obj.configurarOuvinteDeEventos();    
            end
            
            
        end
        
       
        
        function valor=obterValor(obj)
            valor=get(obj.obterId(),'Value');
        end
        
        function valor=obterTexto(obj)
            valor=get(obj.obterId(),'String');
        end
        
        function fixarTexto(obj,texto)
            set(obj.obterId(),'String',texto);
        end
        
        function eVisivel(obj,visivel)
            
            if visivel
                set(obj.obterId(),'Visible','on');
                return;
            end
            
            set(obj.obterId(),'Visible','off');
        end
        
        function fixarHabilitar(obj,hab)
            if hab
                set(obj.obterId(),'Enable','on');
                return;
            end
            
            set(obj.obterId(),'Enable','off');
        end
        
        function fixarAlinhamentoHorizontal(obj,alinhamento)
        set(obj.obterId(),'HorizontalAlignment',alinhamento);
        end
        
   
        
    end
    
    methods(Access='private')
        
        function configurarOuvinteDeEventos(obj)
            
            callback=@(handle,evento)obj.notify('componenteClicado');
            set(obj.obterId(),'Callback',callback);
        
        end
        
    end
end


classdef Eixos<gui.Grafico
    
    
    
    
    methods
        
        function obj=Eixos(pai)
            dados.Parent=pai.obterId();
            dados.Units='pixels';
            id=axes(dados);
            obj@gui.Grafico(id);
        end
        
        function eVisivel(obj,visivel)
            
            if visivel
                set(obj.obterId(),'Visible','on');
                return;
            end
            
            set(obj.obterId(),'Visible','off');
        end
        
    
        
        function fixarPosicao(obj,x,y)
            
            posicao=get(obj.obterId(),'OuterPosition');
            posicao(1)=x;
            posicao(2)=y;
            set(obj.obterId(),'OuterPosition',posicao);
            
        end
        
        function fixarDimensao(obj,x,y)
            posicao=get(obj.obterId(),'OuterPosition');
            posicao(3)=x;
            posicao(4)=y;
            set(obj.obterId(),'OuterPosition',posicao);
            
        end
    end
    
end


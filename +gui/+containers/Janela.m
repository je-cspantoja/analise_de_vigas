classdef Janela<gui.Grafico
    
  
    
    methods
        
        function obj=Janela(titulo)
            dados.NumberTitle='off';
            dados.Name=titulo;
            dados.Renderer='zbuffer';
            dados.Units='pixels';
            dados.Visible='off';
            dados.MenuBar='none';
            dados.Resize='off';
            dados.RendererMode='manual';
            obj@gui.Grafico(figure(dados));
        end
        
        function habilitarBarraDeFerramentasDeFigura(obj,habilitar)
            if habilitar
            set(obj.obterId(),'ToolBar','figure');
            return;
            end
            
            set(obj.obterId(),'ToolBar','none');
        
        end
        
        function fixarRedimensionavel(obj,redimensionavel)
        
            if redimensionavel
                set(obj.obterId(),'Resize','on');
                return;
            end
            set(obj.obterId(),'Resize','off');
            
        end
        
        
        function fixarVisivel(obj,visivel)
            if visivel
            set(obj.obterId(),'Visible','on');
            return;
            end
            set(obj.obterId(),'Visible','off');
            
        end
      
    end
    
end


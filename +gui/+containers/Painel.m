classdef Painel<gui.Componente
    
    
    
    
    methods
        
        function obj=Painel(pai,titulo)
            dados.Units='pixels';
            dados.Title=titulo;
            dados.Parent=pai.obterId();
            obj@gui.Componente(uipanel(dados));
        end
        
        function fixarTexto(obj,texto)
            set(obj.obterId(),'Title',texto);
        end
        
        function t=obterTexto(obj)
            t=get(obj.obterId(),'Title');
        end
        
    end
    
end


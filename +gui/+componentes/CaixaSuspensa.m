classdef CaixaSuspensa<gui.Componente
    
    
    properties(Access='private')
        itemSelecionado;
    end
    
    events
        mudancaDeItemSelecionado;
    end
    
    methods
        function obj=CaixaSuspensa(pai)
            dados.Parent=pai.obterId();
            dados.Style='popupmenu';
            id=uicontrol(dados);
            obj@gui.Componente(id);
            obj.itemSelecionado=obj.obterItemSelecionado();
            
            obj.addlistener('componenteClicado',@obj.mudancaDeValor);
            
        end
        
        function item=obterItemSelecionado(obj)
            item=[];
            itens=obj.obterTexto();
            
            
            
            if ~isempty(itens)
                selecionado=get(obj.obterId(),'Value');
                item=itens{selecionado};
            end
            
            
        end
        
        
        
        function fixarTexto(obj,t)
            
            if ~isempty(t) && iscell(t)
            set(obj.obterId(),'Value',1);
            fixarTexto@gui.Componente(obj,t);
            end
            
         
         obj.mudancaDeValor([],[]);
        end
    end
    
    methods(Access='private')
        function mudancaDeValor(obj,src,evt)
            itemAtual=obj.obterItemSelecionado();
            
            if ~strcmp(obj.itemSelecionado,itemAtual)
                obj.itemSelecionado=itemAtual;
                obj.notify('mudancaDeItemSelecionado');
            end
        end
    end
end


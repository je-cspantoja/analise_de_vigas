classdef BotaoDeAlternancia<gui.Componente
    
  
    
    methods
        function obj=BotaoDeAlternancia(pai)
            dados.Parent=pai.obterId();
            dados.Style='togglebutton';
            id=uicontrol(dados);
            obj@gui.Componente(id);
        end
    end
    
end


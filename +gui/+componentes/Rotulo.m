classdef Rotulo<gui.Componente
    
   
    
    methods
        function obj=Rotulo(pai)
            dados.Parent=pai.obterId();
            dados.Style='text';
            id=uicontrol(dados);
            obj@gui.Componente(id);
            obj.fixarAlinhamentoHorizontal(gui.Componente.esquerda);
        end
    end
    
end


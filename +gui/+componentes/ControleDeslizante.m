classdef ControleDeslizante<gui.Componente
    
    
    methods
        function obj=ControleDeslizante(pai)
            dados.Parent=pai.obterId();
            dados.Style='slider';
            id=uicontrol(dados);
            obj@gui.Componente(id);
        end
    end
    
end


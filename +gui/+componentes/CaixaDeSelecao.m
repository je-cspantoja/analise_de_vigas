classdef CaixaDeSelecao<gui.Componente
    
  
    methods
        function obj= CaixaDeSelecao(pai)
            dados.Parent=pai.obterId();
            dados.Style='checkbox';
            id=uicontrol(dados);
            obj@gui.Componente(id);
        end
    end
    
end


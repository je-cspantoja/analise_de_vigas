classdef CaixaDeListagem<gui.Componente
    
    
   
    
    methods
        function obj=CaixaDeListagem(pai)
            dados.Parent=pai.obterId();
            dados.Style='listbox';
            id=uicontrol(dados);
            obj@gui.Componente(id);
        end
        
        function removerItemSelecionado(obj)
            
            itens=obj.obterTexto();
            
            if ~isempty(itens)
            valor=obj.obterValor();
            
            indices=1:length(itens);
            indices(valor)=[];
            
            novoTamanho=length(itens)-1;
            if novoTamanho==0
                
                obj.fixarTexto('');
                id=obj.obterId();
                set(id,'Value',1);
                
                return;
                
            end
            aux=itens;
            itens=[];
            itens{novoTamanho}=[];
            

            
            for i=1:novoTamanho
            
                itens{i}=aux{indices(i)};
               
            end
            
            obj.fixarTexto(itens);
            id=obj.obterId();
            set(id,'Value',novoTamanho);
        
            end
        
        end
        
    end
    
end


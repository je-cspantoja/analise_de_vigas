classdef (Abstract) Grafico<handle
    
    properties(Access='private')
        id;
    end
    
    methods
        
        function obj=Grafico(id)
            obj.id=id;
        end
        
        
        function fixarPosicao(obj,x,y)
            
            posicao=get(obj.id,'Position');
            posicao(1)=x;
            posicao(2)=y;
            set(obj.id,'Position',posicao);
            
        end
        
        function fixarDimensao(obj,x,y)
            posicao=get(obj.id,'Position');
            posicao(3)=x;
            posicao(4)=y;
            set(obj.id,'Position',posicao);
            
        end
        
        function [x,y]=obterDimensao(obj)
           
            posicao=get(obj.id,'Position');
            x=posicao(3);
            y=posicao(4);
            
        end        
        function i=obterId(obj)
            i=obj.id;
        end
        
        
        
    end
end


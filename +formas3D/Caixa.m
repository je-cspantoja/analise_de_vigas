classdef Caixa<handle
    
    properties
        vertices;
        cor=[0.5 0.5 0.5]   ;
        
    end
    
    properties(Constant)
        
        topo=[1 3 7 5];
        baixo=[8 4 6 2];
        frente=[8 4 5 1];
        tras=[2 6 7 3];
        esquerda=[8 1 3 2];
        direita=[4 5 7 6];
        
    end
    
    methods
        
        function obj=Caixa(varargin)
            
            if nargin==2
                origem=varargin{1};
                dimensao=varargin{2};
                obj.vertices=calcularVertices(origem,dimensao);
            else
                obj.vertices=[];
            end
            
            
            
            
        end
        
        function c=copia(obj)
            c=formas3D.Caixa();
            c.vertices=obj.vertices;
            c.cor=obj.cor;
        end
        
        function parametros_patch = obterParametrosDeIlustracao(obj)
            
            parametros_patch.Vertices=obj.vertices;
            parametros_patch.Faces=obj.obterVetorDeFaces();
            parametros_patch.FaceVertexCData=[obj.cor;obj.cor;obj.cor;obj.cor;obj.cor;obj.cor];
            parametros_patch.FaceColor='flat';
            parametros_patch.LineWidth=0.0001;
            parametros_patch.EdgeColor=obj.cor;
            parametros_patch.EdgeLighting='flat';
            
        end
        
        
        function rotacionarFace(obj,face,rotacoes)
            vetorRotacao=rotacoes;
            coordenadas_a_rotacionar=obj.vertices(face,1:3);
            ptMedio=calcularPontoMedio(coordenadas_a_rotacionar');
            coordenadas_a_rotacionar=moverParaOrigem(coordenadas_a_rotacionar,ptMedio);
            coordenadas_rotacionadas=rotacionarCoordenadas(coordenadas_a_rotacionar,vetorRotacao);
            coordenadas_rotacionadas=moverParaPosicaoRelativaAOrigem(coordenadas_rotacionadas,ptMedio);
            obj.vertices(face,1:3)=coordenadas_rotacionadas;
            
        end
        
        function fixarCor(obj,cor)
            obj.cor=cor;
        end
        
        function  moverFace(obj,face,translacoes)
            
            vetorTranslacoes=translacoes;
            obj.vertices= moverVertices(obj.vertices,face,vetorTranslacoes);
        end
    end
    
    
    
    
    
    methods(Access='private')
        function vetorFaces= obterVetorDeFaces(obj)
            
            vetorFaces=[ ...
                obj.topo; ...
                obj.baixo; ...
                obj.esquerda; ...
                obj.direita; ...
                obj.frente;  ...
                obj.tras ...
                ];
            
        end
        
    end
    
end



function vertices=calcularVertices(pontoDeReferencia,dimensao)
vertices=[ 0 0 1;0 1 0;0 1 1;1 0 0;1 0 1;1 1 0;1 1 1;0 0 0];

dim=[dimensao.x 0 0;0 dimensao.y 0;0 0 dimensao.z];

vertices=vertices*dim;

indices_vertices=1:8;
vetorDeslocamento=[pontoDeReferencia.x,pontoDeReferencia.y,pontoDeReferencia.z];

vertices= moverVertices(vertices,indices_vertices,vetorDeslocamento);
end

function vertices= moverVertices(vertices,indices_vertices,vetorDeslocamento)

linhas=length(indices_vertices);

deslocamentoX=vetorDeslocamento(1);
deslocamentoY=vetorDeslocamento(2);
deslocamentoZ=vetorDeslocamento(3);

vetorDeslocamento=zeros(linhas,3);
vetorDeslocamento(:,1)=deslocamentoX;
vetorDeslocamento(:,2)=deslocamentoY;
vetorDeslocamento(:,3)=deslocamentoZ;

coordenadas_a_deslocar=vertices(indices_vertices,1:3);


coordenadas_a_deslocar=coordenadas_a_deslocar+vetorDeslocamento;
vertices(indices_vertices,1:3)=coordenadas_a_deslocar;

end







function coordenadas=moverParaOrigem(coordenadas,ptMedio)
coordenadas=moverVertices(coordenadas,1:4,-1*ptMedio);

end


function coordenadas=moverParaPosicaoRelativaAOrigem(coordenadas,ptMedio)
coordenadas=moverVertices(coordenadas,1:4,ptMedio);

end

function coordenadas=rotacionarCoordenadas(coordenadas,vetorRotacao)
rotacaoX=vetorRotacao(1);
rotacaoY=vetorRotacao(2);
rotacaoZ=vetorRotacao(3);
[linhas,colunas]=size(coordenadas);

for linha=1:linhas
    ponto=coordenadas(linha,:);
    
    ponto = rotacaoDePonto(ponto,rotacaoX,'x');
    ponto = rotacaoDePonto(ponto,rotacaoY,'y');
    ponto = rotacaoDePonto(ponto,rotacaoZ,'z');
    coordenadas(linha,:)=ponto;
    
end

end

% Dada uma matriz de cooedenadas no formato coordenadas=[x1 x2 ... xn; y1 y2 ... yn; z1 z2 ... zn]
% a funcao retorna as coordenadas do ponto medio entre elas.
function ptMedio=calcularPontoMedio(coordenadas)
[linhas,numCoordenadas]=size(coordenadas);

somaX=sum(coordenadas(1,:));
somaY=sum(coordenadas(2,:));
somaZ=sum(coordenadas(3,:));

x=somaX/numCoordenadas;
y=somaY/numCoordenadas;
z=somaZ/numCoordenadas;
ptMedio=[x y z];
end

function ponto = rotacaoDePonto(ponto,grauRadianos,eixo)

x=ponto(1);
y=ponto(2);
z=ponto(3);



switch eixo
    
    case 'x'
        [y,z]=rotacionar(y,z,grauRadianos);
    case 'y'
        
        
        [x,z]=rotacionar(x,z,grauRadianos);
    case 'z'
        
        [x,y]=rotacionar(x,y,grauRadianos);
       
end

ponto=[x,y,z];

end




function [x,y]=rotacionar(x,y,grauRadianos)
raio=calcularRaio(x,y);
senoA=sin(grauRadianos);
cosA=cos(grauRadianos);

senoB=y/raio;
cosB=x/raio;

x=raio*cossenoSomaDeArcos(senoA,senoB,cosA,cosB);
y=raio*senoSomaDeArcos(senoA,senoB,cosA,cosB);
end

function raio=calcularRaio(a,b)
raio=sqrt(sum([a b].^2));
end

function seno=senoSomaDeArcos(senoA,senoB,cosA,cosB)
seno=senoA*cosB +cosA*senoB;
end

function cosseno=cossenoSomaDeArcos(senoA,senoB,cosA,cosB)
cosseno=cosA*cosB -senoA*senoB;
end



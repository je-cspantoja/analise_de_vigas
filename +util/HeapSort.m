classdef HeapSort<handle
    
    
    
    methods
        
        function ordenar(obj,lista)
        heapsort(lista);
        end
    end
    
end

function heapsort(lista)

funcaoMaiorQue=@maiorQue;
funcaoTrocar=@trocar;
tamanhoDoVetor=lista.totalDeItens();

contruirArvoreHeap(lista,tamanhoDoVetor,funcaoMaiorQue,funcaoTrocar);



while tamanhoDoVetor>=1

trocar(lista,1,tamanhoDoVetor);
tamanhoDoVetor=tamanhoDoVetor-1;

peneiraParaCima(lista,1,tamanhoDoVetor,funcaoMaiorQue,funcaoTrocar);


end





end


function peneiraParaCima(lista,posicaoPai,tamanhoDaLista,funcaoMaiorQue,funcaoTrocar)
posicaoFilhoEsquerdo=2*posicaoPai;
posicaoFilhoDireito=posicaoFilhoEsquerdo+1;

posicaoMaior=posicaoPai;

possuiFilhoEsquerdo=posicaoFilhoEsquerdo<=tamanhoDaLista;

if ~possuiFilhoEsquerdo
    return;
end


if  funcaoMaiorQue(lista,posicaoFilhoEsquerdo,posicaoPai)
    posicaoMaior=posicaoFilhoEsquerdo;
end

possuiFilhoDireito=posicaoFilhoDireito<=tamanhoDaLista;

if  possuiFilhoDireito && funcaoMaiorQue(lista,posicaoFilhoDireito,posicaoMaior)
    posicaoMaior=posicaoFilhoDireito;
end

if posicaoMaior~=posicaoPai
    funcaoTrocar(lista,posicaoPai,posicaoMaior);
    peneiraParaCima(lista,posicaoMaior,tamanhoDaLista,funcaoMaiorQue,funcaoTrocar);
end

end


function contruirArvoreHeap(lista,tamanhoDaLista,funcaoMaiorQue,funcaoTrocar)

i=floor(tamanhoDaLista/2);

while i>=1
    
peneiraParaCima(lista,i,tamanhoDaLista,funcaoMaiorQue,funcaoTrocar);
i=i-1;

end


end

function maior=maiorQue(lista,a,b)
maior=lista.item(a)>lista.item(b);
end

function trocar(lista,a,b)

valorA=lista.item(a);
valorB=lista.item(b);

lista.adicionarItemNaPosicao(a,valorB);
lista.adicionarItemNaPosicao(b,valorA);

end


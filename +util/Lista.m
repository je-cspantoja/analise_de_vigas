classdef Lista<handle
    
    properties
        
        dados;
        contagemDeItens;
        
        
    end
    
    methods
        
        function obj=Lista(tamanho)
            assert(isscalar(tamanho),'O argumento tamanho deve ser escalar');
            assert(isnumeric(tamanho),'O argumento tamanho deve ser um valor numerico');
            assert(tamanho>0,'O tamanho da lista deve ser maior que zero.');
            
            import util.Contador;
            obj.contagemDeItens=Contador(tamanho);
            obj.dados{tamanho}=[];
            
        end
        
        
        function adicionarItemNaPosicao(obj,indice,item)
            obj.validacaoDeIndice(indice);
            obj.dados{indice}=item;
            
        end
        
        function it=item(obj,indice)
            obj.validacaoDeIndice(indice);
            it= obj.dados{indice};
        end
        
        function adicionarItem(obj,item)
            
            obj.validacaoDeItem(item);
            obj.contagemDeItens.incrementar();
            posicao=obj.totalDeItens();
            obj.dados{posicao}=item;
        end
        
        function mat=paraMatriz(obj)
            mat=cell2mat(obj.dados);
        end
        
        function t=totalDeItens(obj)
            
            t=obj.contagemDeItens.contagemAtual();
            
        end
        
        function iter=iterador(obj)
            
            import util.Iterador;
            iter=Iterador(obj);
        end
        
        function ordenar(obj)
            import util.HeapSort;
            hs=HeapSort();
            hs.ordenar(obj);
            
        end
        
        
      
        
    end
    
    
    methods(Access='private')
        
        function vazia=estaVazia(obj)
            vazia=obj.totalDeItens()==0;
        end
        
        function vazia=estaCheia(obj)
            vazia=obj.contagemDeItens.atingiuValorMaximo();
        end
        
        
        function validacaoDeIndice(obj,indice)
            assert(~obj.estaVazia(),'A lista nao deve estar vazia');
            assert(isscalar(indice),'O argumento indice deve ser escalar');
            assert(isnumeric(indice),'O argumento indice deve ser um valor numerico');
            assert(indice<=obj.totalDeItens(),'O argumento indice deve ser menor ou igual ao total de itens armazenados.');
            assert(indice>0,'indice argumentado deve ser maior que zero.');
            
        end
        
        function validacaoDeItem(obj,item)
            
            assert(~isempty(item),'O argumento item nao deve ser vazio.');
            assert(~obj.estaCheia(),'A lista nao deve estar cheia.');
            
            
            
        end
    end
end


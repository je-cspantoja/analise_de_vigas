classdef Iterador<handle
    
    properties
        
        lista;
        contador;
    end
    
    methods
        
        function obj=Iterador(lista)
            import util.Contador;
            
            obj.lista=lista;
            obj.contador=Contador(lista.totalDeItens());
            
        end
        
        function possui=possuiProximo(obj)
            estaNoMaximo=obj.contador.atingiuValorMaximo();
            possui=~estaNoMaximo;
            
        end
        
        
        function proximoItem=proximo(obj)
            assert(obj.possuiProximo,'O iterador deve ter um proximo item');
            
            obj.contador.incrementar();
            indiceItem=obj.contador.contagemAtual();
            proximoItem=obj.lista.item(indiceItem);
        
        end
        
        function i=indiceApontado(obj)
        i=obj.contador.contagemAtual();
        end
        
    end
    
end


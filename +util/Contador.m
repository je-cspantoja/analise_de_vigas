classdef Contador<handle
    
    
    properties
        contagem;
        valorMaximo;
        
    end
    
    methods
        
        function obj=Contador(valorMaximo)
            
            obj.valorMaximo=valorMaximo;
            obj.contagem=0;
        end
        
        
        function incrementou=incrementar(obj)
            
            if ~obj.atingiuValorMaximo()
                
                incrementou=true;
                obj.contagem=obj.contagem+1;
                return;
                
            end
            
            incrementou=false;
            
        end
        
        function decrementou=decrementar(obj)
            aux=obj.contagem-1;
            
            if aux<0
                decrementou=false;
                
                return;
                
            end
            decrementou=true;
            obj.contagem=aux;
            
            
        end
        
        function atingiu=atingiuValorMaximo(obj)
            atingiu=obj.contagem==obj.valorMaximo;
        end
        
        function contagem=contagemAtual(obj)
            
            contagem=obj.contagem;
            
        end
        
        function zerar(obj)
            obj.contagem=0;
        end
        
    end
    
end


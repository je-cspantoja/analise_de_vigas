classdef DiscretizadorLongitudinal<handle
    
    
    properties(Access='private')
        
        yEmFuncaoDeX;
        zEmFuncaoDeX;
        comprimento;
        numeroDeElementos;
        geoms;
        
    end
    
    methods
        
        function obj=DiscretizadorLongitudinal(comprimento,alturas,espessuras,numeroDeElementos)
            
            xAlturas=alturas(:,1);
            yAlturas=alturas(:,2)/2;
            xEspessuras=espessuras(:,1);
            zEspessuras=espessuras(:,2)/2;
            
            ppxy=pchip(xAlturas,yAlturas);
            
            ppxz=pchip(xEspessuras,zEspessuras);
            
            obj.yEmFuncaoDeX=@(x)ppval(ppxy,x);
            obj.zEmFuncaoDeX=@(x)ppval(ppxz,x);
            obj.comprimento=comprimento;
            obj.numeroDeElementos=numeroDeElementos;
            
        end
        
        function c=obterComprimento(obj)
            c=obj.comprimento;
        end
        
        
        function g=criarRepresentacoesGeometricas(obj)
            
            if ~isempty(obj.geoms)
                g=obj.geoms;
                return;
            end
            
            import lva.programa_oo.modelo.geometria.Caixa;
            import lva.programa_oo.modelo.Nodo;
            import util.Contador;
            import util.Lista;
            
            passoX=obj.comprimento/obj.numeroDeElementos;
            dominioX=0:passoX:obj.comprimento;
            
            g=Lista(obj.numeroDeElementos);
            contador=Contador(obj.numeroDeElementos);
            
            while contador.incrementar()
                
                i=contador.contagemAtual();
                x1=dominioX(i);
                y1=obj.yEmFuncaoDeX(x1);
                z1=obj.zEmFuncaoDeX(x1);
                
                x2=dominioX(i+1);
                y2=obj.yEmFuncaoDeX(x2);
                z2=obj.zEmFuncaoDeX(x2);
                
                origem=[];
                dimensao=[];
                
                dimensao.x=x2-x1;
                dimensao.y=(y2+y1);
                dimensao.z=(z2+z1);
                
                origem.x=x1;
                origem.y=-(dimensao.y/2);
                origem.z=-(dimensao.z/2);
                
                rpGeo=Caixa(origem,dimensao);
                g.adicionarItem(rpGeo);
            end
            
            obj.geoms=g;
        end
        
        function nodos=criarNodos(obj,tipoMovimento)
            import util.Lista;
            import util.Contador;
            
            passoX=obj.comprimento/obj.numeroDeElementos;
            numeroDeNos=obj.numeroDeElementos+1;
            nodos=Lista(numeroDeNos);
            contador=Contador(obj.numeroDeElementos);
            
            while contador.incrementar()
                
                i=contador.contagemAtual();
                x=(i-1)*passoX;
                n=tipoMovimento.criarNodo(i,struct('x',x,'y',0,'z',0));
                nodos.adicionarItem(n);
                
            end
            
            n=tipoMovimento.criarNodo(numeroDeNos,struct('x',obj.comprimento,'y',0,'z',0));
            nodos.adicionarItem(n);
            
        end
        
        function elementos=criarElementos(obj,geoms,nodos,material,tipoMovimento)
            iterador=geoms.iterador();
            
            elementos=util.Lista(geoms.totalDeItens());
            
            while iterador.possuiProximo()
                
                geom=iterador.proximo();
                id=iterador.indiceApontado();
                nodoEsquerdo=nodos.item(id);
                nodoDireito=nodos.item(id+1);
                
                el=tipoMovimento.criarElemento(id,nodoEsquerdo,nodoDireito,geom, material);
                elementos.adicionarItem(el);
                
            end
            
        end
        
    end
    
    
    
end



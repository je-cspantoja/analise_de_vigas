classdef PerfilTorcao<lva.programa_oo.modelo.movimentos.PerfilDoMovimento
    
    methods(Access='protected')
        
        
        
        function criarOpcoesDeContorno(obj)
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            obj.opcoesDeContorno={TiposDeContorno.LIVRE,TiposDeContorno.ENGASTADO};
        end
        
        function criarMovimentosNodais(obj)
            import lva.programa_oo.modelo.MovimentosIndependentes;
            
            obj.movimentosNodais={MovimentosIndependentes.ROTACAO_X};
        end
        
        function criarConstrutorDeElemento(obj)
            import lva.programa_oo.modelo.elementos.viga.VigaTorcao;
            obj.construtorElemento=@VigaTorcao;
        end
        
        function criarConstrutorRestritorDeMovimento(obj)
            import lva.programa_oo.modelo.restritoresDeMovimento.PerfilRestritorTorcao
            perfil=PerfilRestritorTorcao();
            obj.construtorRestritorDeMovimento=@perfil.instanciarRestritor;
        end
        
        function criarAnalistaDoProcessamento(obj)
            import lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamentoTorcao;
            
            obj.analistaDoProcessamento=AnalistaDoProcessamentoTorcao();
            
        end
        
    end
    
end


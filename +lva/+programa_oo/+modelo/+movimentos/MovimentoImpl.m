classdef MovimentoImpl<lva.programa_oo.modelo.movimentos.Movimento
    
    properties(Access='private')
        opcoesDeContorno;
        movimentosNodais;
        construtorElemento;
        construtorRestritorDeMovimento;
        analisadorDeProcessamento;
    end
    
    methods
        
        
        function obj=MovimentoImpl(opcoesDeContorno,movimentosNodais,construtorElemento,construtorRestritorDeMovimento,analisadorDeProcessamento)
            
            obj.opcoesDeContorno=opcoesDeContorno;
            obj.movimentosNodais=movimentosNodais;
            obj.construtorElemento=construtorElemento;
            obj.construtorRestritorDeMovimento=construtorRestritorDeMovimento;
            obj.analisadorDeProcessamento=analisadorDeProcessamento;
        end
        
        function valido=contornoValido(obj,opc)
            
            switch(opc)
                
                case obj.opcoesDeContorno
                    valido=true;
                otherwise
                    valido=false;
                    
            end
            
        end
        
        function opcoes=obterOpcoesDeContorno(obj)
            opcoes=obj.opcoesDeContorno;
        end
        
        function elemento=criarElemento(obj,id,nodoEsquerdo,nodoDireito,geometria, material)
            import util.Lista;
            nodos=Lista(2);
            nodos.adicionarItem(nodoEsquerdo);
            nodos.adicionarItem(nodoDireito);
            elemento=obj.construtorElemento(id,nodos,geometria, material);
        end
        
        function n=criarNodo(obj,id,coordenadas)
            import lva.programa_oo.modelo.Nodo;
            n=Nodo(id,coordenadas,obj.movimentosNodais);
        end
        
        function ordem=calcularOrdenDaMatrizEstrutural(obj,numeroDeNos)
            grauDeLiberdadePorNo=length(obj.movimentosNodais);
            ordem=grauDeLiberdadePorNo*numeroDeNos;
        end
        
        
        function solucao=solucionar(obj,elementos,nodos,condicaoDeContornoEsq,condicaoDeContornoDir)
            import  lva.programa_oo.modelo.montadoresDeMatrizEstrutural.MontadorDeMatrizDeMassa;
            import lva.programa_oo.modelo.montadoresDeMatrizEstrutural.MontadorDeMatrizDeRigidez;
            import lva.programa_oo.modelo.solucionadores.SolucionadorAnaliseModal;
            
            
            ordemMatrizEstrutural=obj.calcularOrdenDaMatrizEstrutural(nodos.totalDeItens());
            montadorMassa=MontadorDeMatrizDeMassa(ordemMatrizEstrutural,elementos);
            montadorRigidez=MontadorDeMatrizDeRigidez(ordemMatrizEstrutural,elementos);
            restritor=obj.construtorRestritorDeMovimento(nodos,condicaoDeContornoEsq,condicaoDeContornoDir);
            solucionador=SolucionadorAnaliseModal(montadorMassa,montadorRigidez,restritor);
            solucao=solucionador.solucionar();
              
        end

        function modos=gerarModosDeVibracao(obj,solucao)
            modos=obj.analisadorDeProcessamento.gerarModosDeVibracao(solucao);
        end
    end
end


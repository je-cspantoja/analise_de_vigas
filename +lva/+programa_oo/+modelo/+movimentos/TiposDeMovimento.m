classdef TiposDeMovimento<lva.programa_oo.modelo.movimentos.Movimento
    
    enumeration
        
        AXIAL(lva.programa_oo.modelo.movimentos.PerfilAxial());
        FLEXAO_EM_Y(lva.programa_oo.modelo.movimentos.PerfilFlexaoY());
        FLEXAO_EM_Z(lva.programa_oo.modelo.movimentos.PerfilFlexaoZ());
        TORCAO(lva.programa_oo.modelo.movimentos.PerfilTorcao());
        
    end
    
    properties(SetAccess='immutable')
        instancia;
    end
    
    methods
        
        function obj=TiposDeMovimento(perfilDoMovimento)
            obj.instancia=perfilDoMovimento.instanciarMovimento();
        end
        
        function  valido=contornoValido(obj,opc)
            valido=obj.instancia.contornoValido(opc);
        end
        
        function  elemento=criarElemento(obj,id,nodoEsquerdo,nodoDireito,geometria, material)
            elemento=obj.instancia.criarElemento(id,nodoEsquerdo,nodoDireito,geometria, material);
        end
        
        function  n=criarNodo(obj,id,coordenadas)
            n=obj.instancia.criarNodo(id,coordenadas);
        end
        
        function ordem=calcularOrdenDaMatrizEstrutural(obj,numeroDeNos)
            ordem=obj.instancia.calcularOrdenDaMatrizEstrutural(numeroDeNos);
        end
        
        
        function opcoes=obterOpcoesDeContorno(obj)
            opcoes=obj.instancia.obterOpcoesDeContorno();
        end
        
        function   solucao=solucionar(obj,elementos,nodos,condicaoContornoEsq,condicaoContornoDir)
            solucao=obj.instancia.solucionar(elementos,nodos,condicaoContornoEsq,condicaoContornoDir);
        end
        
        function modos=gerarModosDeVibracao(obj,solucao)
            modos=obj.instancia.gerarModosDeVibracao(solucao);
        end
        
    end
    
end
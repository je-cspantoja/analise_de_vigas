classdef (Abstract)Movimento<handle
    
     
    methods
        
       
        elemento=criarElemento(obj,id,nodoEsquerdo,nodoDireito,geometria, material);
        valido=contornoValido(obj,opc);
        opcoes=obterOpcoesDeContorno(obj);
        n=criarNodo(obj,id,coordenadas);
        ordem=calcularOrdenDaMatrizEstrutural(obj,numeroDeNos);
        solucao=solucionar(obj,elementos,nodos,condicaoContornoEsq,condicaoContornoDir);
        modos=gerarModosDeVibracao(obj,solucao);
        
    end
end


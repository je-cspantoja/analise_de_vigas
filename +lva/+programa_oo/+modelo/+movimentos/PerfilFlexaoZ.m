classdef PerfilFlexaoZ<lva.programa_oo.modelo.movimentos.PerfilDoMovimento
    
    methods(Access='protected')
        
        function criarOpcoesDeContorno(obj)
            
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            obj.opcoesDeContorno={TiposDeContorno.LIVRE,TiposDeContorno.GUIADO,TiposDeContorno.APOIADO,TiposDeContorno.ENGASTADO};
        end
        
        function criarMovimentosNodais(obj)
            import lva.programa_oo.modelo.MovimentosIndependentes;
            obj.movimentosNodais={MovimentosIndependentes.DESCOLOCAMENTO_Z,MovimentosIndependentes.ROTACAO_Y};
        end
        
        function criarConstrutorDeElemento(obj)
            import lva.programa_oo.modelo.elementos.viga.VigaFlexao;
            obj.construtorElemento=@(id,nodos,geometria, material)VigaFlexao(id,nodos,geometria, material,'z');
        end
        
        function criarConstrutorRestritorDeMovimento(obj)
            import lva.programa_oo.modelo.restritoresDeMovimento.PerfilRestritorFlexaoZ
            perfil=PerfilRestritorFlexaoZ();
            obj.construtorRestritorDeMovimento=@perfil.instanciarRestritor;
        end
        
        function criarAnalistaDoProcessamento(obj)
            import lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamentoFlexao;
            
            obj.analistaDoProcessamento=AnalistaDoProcessamentoFlexao('z');
            
        end
    end
    
end


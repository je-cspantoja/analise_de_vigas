classdef (Abstract)PerfilDoMovimento<handle
    
    
    properties(Access='protected')
        
        opcoesDeContorno;
        movimentosNodais;
        construtorElemento;
        construtorRestritorDeMovimento;
        analistaDoProcessamento;
    
    end
    
    methods(Access='protected')
        
        criarOpcoesDeContorno(obj);
        criarMovimentosNodais(obj);
        criarConstrutorDeElemento(obj);
        criarConstrutorRestritorDeMovimento(obj);
        criarAnalistaDoProcessamento(obj);
        
    end
    
    methods
        
        function mov=instanciarMovimento(obj)
            import lva.programa_oo.modelo.movimentos.MovimentoImpl;
            obj.criarOpcoesDeContorno();
            obj.criarMovimentosNodais();
            obj.criarConstrutorDeElemento();
            obj.criarConstrutorRestritorDeMovimento();
            obj.criarAnalistaDoProcessamento();
            
            mov=MovimentoImpl(obj.opcoesDeContorno,obj.movimentosNodais,obj.construtorElemento,obj.construtorRestritorDeMovimento,obj.analistaDoProcessamento);
        end
        
    end
    
end


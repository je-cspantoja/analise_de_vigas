classdef PerfilAxial<lva.programa_oo.modelo.movimentos.PerfilDoMovimento
    
    
    methods(Access='protected')
        
        function criarOpcoesDeContorno(obj)
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            obj.opcoesDeContorno={TiposDeContorno.LIVRE,TiposDeContorno.ENGASTADO};
        end
        
        function criarMovimentosNodais(obj)
            import lva.programa_oo.modelo.MovimentosIndependentes;
            obj.movimentosNodais={MovimentosIndependentes.DESCOLOCAMENTO_X};
        end
        
        function criarConstrutorDeElemento(obj)
            import lva.programa_oo.modelo.elementos.viga.VigaAxial;
            obj.construtorElemento=@VigaAxial;
        end
        
        function criarConstrutorRestritorDeMovimento(obj)
            import lva.programa_oo.modelo.restritoresDeMovimento.PerfilRestritorAxial
            perfil=PerfilRestritorAxial();
            obj.construtorRestritorDeMovimento=@perfil.instanciarRestritor;
        end
        
        function criarAnalistaDoProcessamento(obj)
            import lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamentoAxial;
            
            obj.analistaDoProcessamento=AnalistaDoProcessamentoAxial();
            
        end
    end
    
end


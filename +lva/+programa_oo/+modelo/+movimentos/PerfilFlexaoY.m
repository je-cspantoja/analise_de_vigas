classdef PerfilFlexaoY<lva.programa_oo.modelo.movimentos.PerfilDoMovimento
    
    methods(Access='protected')
        
        function criarOpcoesDeContorno(obj)
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            obj.opcoesDeContorno={TiposDeContorno.LIVRE,TiposDeContorno.GUIADO,TiposDeContorno.APOIADO,TiposDeContorno.ENGASTADO};
        end
        
        function criarMovimentosNodais(obj)
            import lva.programa_oo.modelo.MovimentosIndependentes;
            obj.movimentosNodais={MovimentosIndependentes.DESCOLOCAMENTO_Y,MovimentosIndependentes.ROTACAO_Z};
        end
        
        function criarConstrutorDeElemento(obj)
            import lva.programa_oo.modelo.elementos.viga.VigaFlexao;
            obj.construtorElemento=@(id,nodos,geometria, material)VigaFlexao(id,nodos,geometria, material,'y');
        end
        
        function criarConstrutorRestritorDeMovimento(obj)
            import lva.programa_oo.modelo.restritoresDeMovimento.PerfilRestritorFlexaoY
            perfil=PerfilRestritorFlexaoY();
            obj.construtorRestritorDeMovimento=@perfil.instanciarRestritor;
        end
        
        function criarAnalistaDoProcessamento(obj)
            import lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamentoFlexao;
            
            obj.analistaDoProcessamento=AnalistaDoProcessamentoFlexao('y');
            
        end
    end
end


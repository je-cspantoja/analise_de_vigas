classdef SolucionadorAnaliseModal<handle
    
    properties(Access='private')
        
        montadorMatrizDeMassa;
        montadorMatrizDeRigidez;
        restritorDeMovimento;
        
    end
    
    methods
        
        
        
        function obj=SolucionadorAnaliseModal(montadorMatrizDeMassa,montadorMatrizDeRigidez,restritorDeMovimento)
            
            
            obj.montadorMatrizDeMassa=montadorMatrizDeMassa;
            obj.montadorMatrizDeRigidez=montadorMatrizDeRigidez;
            obj.restritorDeMovimento=restritorDeMovimento;
            
        end
        
        function solucao=solucionar(obj)
            
            mRigidez=obj.montadorMatrizDeRigidez.montarMatriz();
            mMassa=obj.montadorMatrizDeMassa.montarMatriz();
            [velocidadeAngularQuadratica,movimentos]=obj.solucionarAutoValoresEAutoVetores(mMassa,mRigidez);
            frequencias=obj.calcularFrequencias(velocidadeAngularQuadratica);
            solucao=struct('frequencias',frequencias,'movimentos',movimentos);
            solucao=obj.ordenarPorFrequenciaAscendente(solucao);
            
        end
        
        
        
        
    end
    
    methods(Access='private')
        
        function frequencias=calcularFrequencias(obj,velocidadeAngularQuadratica)
            
            frequencias=sqrt(velocidadeAngularQuadratica)/(2*pi);
            
        end
        
        
        
        function solucao=ordenarPorFrequenciaAscendente(obj,solucao)
            
            [frequenciasPosOrdenacao,indicesPreOrdenacao]=sort(solucao.frequencias);
            movimentosPosOrdenacao=solucao.movimentos(:,indicesPreOrdenacao);
            solucao.frequencias=frequenciasPosOrdenacao;
            solucao.movimentos=movimentosPosOrdenacao;
            
        end
        
        
        function [velocidadeAngularQuadratica,movimentos]=solucionarAutoValoresEAutoVetores(obj,mMassa,mRigidez)
            
            mMassa=obj.restritorDeMovimento.restringir(mMassa);
            mRigidez=obj.restritorDeMovimento.restringir(mRigidez);
            
            [movimentos,velocidadeAngularQuadratica]=eig(mRigidez,mMassa);
            movimentos=obj.restritorDeMovimento.corrigirMovimentosRetringidos(movimentos);
            velocidadeAngularQuadratica=diag(velocidadeAngularQuadratica);
        end
    end
end


classdef (Abstract)PerfilDoRestritor<handle

properties(Access='protected')
mapaTipoContornoEContorno;
grauDeLiberdade;

end

methods

function r=instanciarRestritor(obj,nodos,condicaoContornoEsq,condicaoContornoDir)
import lva.programa_oo.modelo.restritoresDeMovimento.Restritor; 
obj.criarMapaTipoContornoEContorno();
r=Restritor(obj.mapaTipoContornoEContorno,nodos,condicaoContornoEsq,condicaoContornoDir,obj.grauDeLiberdade);
end

end


methods(Access='protected')
criarMapaTipoContornoEContorno(obj);
end

end


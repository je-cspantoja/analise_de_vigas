classdef PerfilRestritorAxial<lva.programa_oo.modelo.restritoresDeMovimento.PerfilDoRestritor
    
    methods(Access='protected')
        function criarMapaTipoContornoEContorno(obj)
            
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            import lva.programa_oo.modelo.MovimentosIndependentes;
            import lva.programa_oo.modelo.condicoesDeContorno.Contorno;
            
            obj.grauDeLiberdade=1;
            
            
            mascaraContornoLivre.movimentoIndependente=MovimentosIndependentes.DESCOLOCAMENTO_X;
            mascaraContornoLivre.restrito=true;
            
            mascaraContornoEngastado.movimentoIndependente=MovimentosIndependentes.DESCOLOCAMENTO_X;
            mascaraContornoEngastado.restrito=false;
            
            obj.mapaTipoContornoEContorno=containers.Map;
            
            livre=Contorno({mascaraContornoLivre});
            engastado=Contorno({mascaraContornoEngastado});
            
            obj.mapaTipoContornoEContorno(TiposDeContorno.LIVRE.char)=livre;
            obj.mapaTipoContornoEContorno(TiposDeContorno.ENGASTADO.char)=engastado;
            
        end
    end
    
end


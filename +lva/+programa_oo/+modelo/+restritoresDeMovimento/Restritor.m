classdef Restritor<handle
    
    properties(Access='private')
        mapaTipoContornoEContorno;
        vetorDeRestricao;
    end
    
    methods
        function obj=Restritor(mapaTipoContornoEContorno,nodos,condicaoContornoEsq,condicaoContornoDir,grauDeLiberdade)
            
            obj.mapaTipoContornoEContorno=mapaTipoContornoEContorno;
            numeroDeNodos=nodos.totalDeItens();
            assert(numeroDeNodos>=2,'O numero de nodos deve ser no minimo 2 para existir um elemento tipo viga');
            obj.vetorDeRestricao=logical(ones(1,numeroDeNodos*grauDeLiberdade));
            noExtremidadeEsquerda=nodos.item(1);
            noExtremidadeDireita=nodos.item(numeroDeNodos);
            contornoEsq=mapaTipoContornoEContorno(condicaoContornoEsq.char);
            contornoDir=mapaTipoContornoEContorno(condicaoContornoDir.char);
            noExtremidadeEsquerda.aplicarContorno(contornoEsq);
            noExtremidadeDireita.aplicarContorno(contornoDir);
            
            dadosContornoEsq=noExtremidadeEsquerda.obterDadosDeContorno();
            dadosContornoDir=noExtremidadeDireita.obterDadosDeContorno();
            
          obj.atualizarVetorDeRestricao(dadosContornoEsq);
          obj.atualizarVetorDeRestricao(dadosContornoDir);
         
        end
        
        function matrizEstrutural=restringir(obj,matrizEstrutural)
            matrizEstrutural=matrizEstrutural(obj.vetorDeRestricao,obj.vetorDeRestricao);
        end
        
        function movimentos=corrigirMovimentosRetringidos(obj,movimentos)
        numeroDeMovimentos=length(obj.vetorDeRestricao);
        dimMov=size(movimentos);
        
        numeroDeModos=dimMov(2);
        
        aux=zeros(numeroDeMovimentos,numeroDeModos);
        
        aux(obj.vetorDeRestricao,:)=movimentos;
        movimentos=aux;
        end
        
        
    end
    
    methods (Access='private')
        
        function atualizarVetorDeRestricao(obj,dadosContorno)
            
            for i=1:length(dadosContorno)
                info=dadosContorno{i};
                obj.vetorDeRestricao(info.posicao)=info.restrito;
            end
        end
    end
end


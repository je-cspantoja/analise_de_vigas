classdef PerfilRestritorFlexaoZ<lva.programa_oo.modelo.restritoresDeMovimento.PerfilDoRestritor
   
    methods(Access='protected')
        function criarMapaTipoContornoEContorno(obj)
            
            import lva.programa_oo.modelo.condicoesDeContorno.TiposDeContorno;
            obj.grauDeLiberdade=2;
            
            obj.mapaTipoContornoEContorno=containers.Map;
                
            livre=criarContorno(true,true);
            guiado=criarContorno(true,false);
            apoiado=criarContorno(false,true);
            engastado=criarContorno(false,false);
            
            obj.mapaTipoContornoEContorno(TiposDeContorno.LIVRE.char)=livre;
            obj.mapaTipoContornoEContorno(TiposDeContorno.GUIADO.char)=guiado;
            obj.mapaTipoContornoEContorno(TiposDeContorno.APOIADO.char)=apoiado;
            obj.mapaTipoContornoEContorno(TiposDeContorno.ENGASTADO.char)=engastado;
        end
    end
    
end

function contorno=criarContorno(deslocamento,rotacao)

import lva.programa_oo.modelo.MovimentosIndependentes;
import lva.programa_oo.modelo.condicoesDeContorno.Contorno;


m{2}=struct('movimentoIndependente',[],'restrito',[]);
m{1}.movimentoIndependente=MovimentosIndependentes.DESCOLOCAMENTO_Z;
m{1}.restrito=deslocamento;

m{2}.movimentoIndependente=MovimentosIndependentes.ROTACAO_Y;
m{2}.restrito=rotacao;
contorno=Contorno(m);

end

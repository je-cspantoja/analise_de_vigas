classdef Processamento<handle



    properties(Access='private')
        condContornoFaceEsquerda;
        condContornoFaceDireita;
        tipoMovimento;
        geometria;
        enderecoResultadoProcessamento;
        material;
        msgErro;
        discretizador;
    end


    methods

        function obj=Processamento(tipoMovimento,condContornoFaceEsquerda,condContornoFaceDireita,material)
            obj.tipoMovimento=tipoMovimento;
            obj.condContornoFaceEsquerda=condContornoFaceEsquerda;
            obj.condContornoFaceDireita=condContornoFaceDireita;
            obj.material=material;
        end


        function tm=obterTipoDeMovimento(obj)
            tm=obj.tipoMovimento;
        end

        function g=carregarArquivoDePreProcessamento(obj,endereco)
            dados=load(endereco);
            assert(isfield(dados,'discretizador'),'Arquivo possui formato nao suportado.');
            obj.discretizador=dados.discretizador;
            obj.geometria=obj.discretizador.criarRepresentacoesGeometricas();
            g=obj.geometria;
        end

        function fixarTipoDeMovimento(obj,tipoMovimento)
            eValido=isa(tipoMovimento,'lva.programa_oo.modelo.movimentos.TiposDeMovimento');
            assert(eValido,'Tipo de movimento invalido');
            obj.tipoMovimento=tipoMovimento;
        end

        function fixarMaterial(obj,material)
            obj.material=material;
        end

        function fixarContornoFaceEsquerda(obj,contorno)
            eValido=obj.tipoMovimento.contornoValido(contorno);
            assert(eValido,'Opcao de contorno invalida');
            obj.condContornoFaceEsquerda=contorno;
        end

        function fixarContornoFaceDireita(obj,contorno)
            eValido=obj.tipoMovimento.contornoValido(contorno);
            assert(eValido,'Opcao de contorno invalida');
            obj.condContornoFaceDireita=contorno;
        end

        function fixarArquivoDeProcessamento(obj,endereco)
            obj.enderecoResultadoProcessamento=endereco;
        end

        function processar(obj)
            assert(~isempty(obj.discretizador),'Nenhum arquivo de pre-processamento foi informado');
            assert(~isempty(obj.enderecoResultadoProcessamento),'O nome do arquivo de saida e obrigatorio');

            nodos=obj.discretizador.criarNodos(obj.tipoMovimento);
            elementos=obj.discretizador.criarElementos(obj.geometria,nodos,obj.material,obj.tipoMovimento);

            solucao=obj.tipoMovimento.solucionar(elementos,nodos,obj.condContornoFaceEsquerda,obj.condContornoFaceDireita);

            solucao.material=obj.material;
            solucao.condContornoFaceEsquerda=obj.condContornoFaceEsquerda;
            solucao.condContornoFaceDireita=obj.condContornoFaceDireita;
            solucao.geometria=obj.geometria;
            solucao.tipoMovimento=obj.tipoMovimento;
            solucao.comprimentoDaEstrutura=obj.discretizador.obterComprimento();

            save(obj.enderecoResultadoProcessamento,'solucao');

        end
    end
end


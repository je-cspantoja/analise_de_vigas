classdef PosProcessamento<handle

    properties
        modos;
        geometria;
        solucao;
    end

    methods

        function [escalaDesl,escalaRot]=carregarArquivoProcessamento(obj,endereco)
            dados=load(endereco);
            assert(isfield(dados,'solucao'),'Arquivo possui formato nao suportado.');
            assert(isfield(dados.solucao,'geometria'),'Arquivo possui formato nao suportado.');
            assert(isfield(dados.solucao,'tipoMovimento'),'Arquivo possui formato nao suportado.');
            assert(isfield(dados.solucao,'comprimentoDaEstrutura'),'Arquivo possui formato nao suportado.');
            obj.solucao=dados.solucao;
            obj.geometria=dados.solucao.geometria;
            tipoMovimento=dados.solucao.tipoMovimento;
            obj.modos=tipoMovimento.gerarModosDeVibracao(dados.solucao);
            comprimentoDaEstrutura=dados.solucao.comprimentoDaEstrutura;
            escalaDesl=comprimentoDaEstrutura/30;
            escalaRot=deg2rad(45);
        end

        function m= obterModo(obj,num)
            m=obj.modos{num};
        end

        function  geom=obterGeometria(obj)
            geom=obj.geometria;
        end

        function num=obterNumeroDeModos(obj)
            num=length(obj.modos);
        end

        function infoModos=obterListaDeFrequencias(obj)
            numModos=obj.obterNumeroDeModos();
            infoModos{numModos}=[];

            for i=1:numModos
                infoModos{i}=sprintf('Modo %d: Frequência natural %d Hz',i,obj.modos{i}.frequencia);
             end

        end

    end

end


classdef PreProcessamento<handle


    properties(Access='private')
        dimensao;
        faceEsquerda;
        faceDireita;
        comprimento;
        alturas;
        espessuras;
        nomeArquivoSaida;
    end

    methods

        function fixarDimensao(obj,dimensao)
            obj.dimensao=dimensao;
        end

        function fixarDimensoesDaFaceEsquerda(obj,face)
            obj.faceEsquerda=face;
        end

        function fixarDimensoesDaFaceDireita(obj,face)
            obj.faceDireita=face;
        end

        function fixarComprimento(obj,comprimento)
            obj.comprimento=comprimento;
        end

        function adicionarEspessuraDadoComprimento(obj,comprimento,espessura)
            obj.espessuras=[obj.espessuras;comprimento,espessura];
        end

        function adicionarAlturaDadoComprimento(obj,comprimento,altura)
            obj.alturas=[obj.alturas;comprimento,altura];
        end

        function fixarArquivoSaida(obj,nomeArquivoSaida)
            obj.nomeArquivoSaida=nomeArquivoSaida;
        end

        function geoms=discretizar(obj,numeroDeElementos)
            fe=obj.faceEsquerda;
            fd=obj.faceDireita;
            comp=obj.comprimento;

            obj.validaLocalDeSalvamento(obj.nomeArquivoSaida);
            obj.validaNumeroDeElementos(numeroDeElementos);
            obj.validaComprimento(comp);
            obj.validaFace(fe,comp);
            obj.validaFace(fd,comp);


            xy=[obj.alturas; [0 fe.altura];[comp fd.altura]];
            xz=[obj.espessuras; [0 fe.espessura];[comp fd.espessura]];

            import lva.programa_oo.modelo.discretizador.DiscretizadorLongitudinal;
            discretizador=DiscretizadorLongitudinal(comp,xy,xz,numeroDeElementos);
            geoms=discretizador.criarRepresentacoesGeometricas();
            save(obj.nomeArquivoSaida,'discretizador');

        end
    end

    methods(Access='private')

        function validaLocalDeSalvamento(obj,local)
            assert(~isempty(local),'Insira o nome do arquivo de pre-processamento');
        end
        function validaNumeroDeElementos(obj,ne)
            assert(ne>0,'O numero de elementos deve ser maior que zero');
        end

        function validaComprimento(obj,comprimento)
            assert(comprimento>0,'O valor do comprimento deve ser maior que zero');
        end

        function validaFace(obj,face,comprimento)
            obj.validaDimensoesRelativasAoComprimento(face.espessura,comprimento);
            obj.validaDimensoesRelativasAoComprimento(face.altura,comprimento);
        end

        function validaDimensoesRelativasAoComprimento(obj,dim,comprimento)
            assert(dim>0&&dim<=(comprimento/5),'As dimensões nos eixos y e z devem ser maiores que zero e no maximo um quinto do comprimento da estrutura para se obter uma boa aproximacao usando elemento tipo viga');
        end
    end
end


classdef Montador<handle
    
    
    properties(Access='private')
        ordemDaMatrizGlobal;
        elementos;
    end
    
    methods
        
        function obj=Montador(ordemDaMatrizGlobal,elementos)
            assert(isa(elementos,'util.Lista'),'O argumento elementos deve ser uma instancia da classe util.Lista');
            obj.ordemDaMatrizGlobal=ordemDaMatrizGlobal;
            obj.elementos=elementos;
            
        end
        
        
        
        function matrizGlobal=montarMatriz(obj)
           
            
            matrizGlobal=zeros(obj.ordemDaMatrizGlobal);
            iterador=obj.elementos.iterador();
            
            while iterador.possuiProximo()
                
                elemento=iterador.proximo();
                infoMontagem=obj.obterMatrizDePropriedades(elemento);
                matrizAuxiliar=matrizGlobal(infoMontagem.linhas,infoMontagem.colunas);
                matrizAuxiliar=matrizAuxiliar+infoMontagem.matriz;
                matrizGlobal(infoMontagem.linhas,infoMontagem.colunas)=matrizAuxiliar;
                
            end
            
            
        end
        
    end
    
    methods(Abstract,Access='protected')
        
        infoMontagem=obterMatrizDePropriedades(obj,elemento);
        
    end
    
end

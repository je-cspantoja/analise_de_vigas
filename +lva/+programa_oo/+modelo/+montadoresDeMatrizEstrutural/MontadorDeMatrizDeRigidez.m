classdef MontadorDeMatrizDeRigidez<lva.programa_oo.modelo.montadoresDeMatrizEstrutural.Montador
    
    
    methods
        
        function obj=MontadorDeMatrizDeRigidez(ordemDaMatrizGlobal,elementos)
            
            obj@lva.programa_oo.modelo.montadoresDeMatrizEstrutural.Montador(ordemDaMatrizGlobal,elementos);
            
        end
        
    end
    
    
    methods(Access='protected')
        
        function   infoMatriz=obterMatrizDePropriedades(obj,elemento)
        
            infoMatriz=elemento.obterMatrizDeRigidez();
        
        end
        
    end
    
end


classdef MontadorDeMatrizDeMassa<lva.programa_oo.modelo.montadoresDeMatrizEstrutural.Montador
    
    
    methods
        
        function obj=MontadorDeMatrizDeMassa(ordemDaMatrizGlobal,elementos)
            
            obj@lva.programa_oo.modelo.montadoresDeMatrizEstrutural.Montador(ordemDaMatrizGlobal,elementos);
            
        end
        
    end
    
    
    methods(Access='protected')
        
        function   infoMatriz=obterMatrizDePropriedades(obj,elemento)
           
            infoMatriz=elemento.obterMatrizDeMassa();
        
        end
        
    end
    
end


classdef MovimentosIndependentes
 
    enumeration
        
     DESCOLOCAMENTO_X;
     DESCOLOCAMENTO_Y;
     DESCOLOCAMENTO_Z;
     
     ROTACAO_X;
     ROTACAO_Y;
     ROTACAO_Z;
     
    end

end


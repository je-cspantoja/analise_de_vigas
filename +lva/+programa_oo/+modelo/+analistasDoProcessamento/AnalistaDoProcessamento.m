classdef (Abstract)AnalistaDoProcessamento<handle
    
    
    methods
        
        
        function modos=gerarModosDeVibracao(obj,solucao)
            
            totalDeModos=length(solucao.frequencias);
            modos{totalDeModos}=[];
            
            for numModo=1:totalDeModos
                
                movimentosDoModo=solucao.movimentos(:,numModo);
                [tipoMovIndependente,movsIndependentes,indiceMovCalcGrauDeVibracao]=obj.separarMovimentosIdependentes(movimentosDoModo);
                grauDeVibracao=obj.calcularGrauDeVibracao(movsIndependentes,indiceMovCalcGrauDeVibracao);
                movsIndependentes=obj.normalizarCelulas(movsIndependentes);
                grauDeVibracao=obj.normalizarVetor(grauDeVibracao);
                infoElementos=obj.gerarInformacoesPorElemento(tipoMovIndependente,movsIndependentes,grauDeVibracao);
                modo.frequencia=solucao.frequencias(numModo);
                modo.infoElementos=infoElementos;
                modos{numModo}=modo;
                
            end
            
        end
        
    end
    
    
    methods(Access='private')
        function movsIndependentes=normalizarCelulas(obj,movsIndependentes)
            
            for i=1:length(movsIndependentes)
                movsIndependentes{i}=obj.normalizarVetor(movsIndependentes{i});
            end
            
        end
        
        
        function vetor=normalizarVetor(obj,vetor)
            valorMaximo=max(vetor);
            valorMinimo=min(vetor);
            
            valorMaximo=abs(valorMaximo);
            valorMinimo=abs(valorMinimo);
            
            maiorValorAbsoluto=valorMaximo;
            
            if valorMinimo>maiorValorAbsoluto;
                maiorValorAbsoluto=valorMinimo;
            end
            
            vetor=vetor/(maiorValorAbsoluto);
        end
        
        function grauDeVibracao=calcularGrauDeVibracao(obj,movsIndependentes,indiceMovCalcGrauDeVibracao)
            
            movimentos=movsIndependentes{indiceMovCalcGrauDeVibracao};
            [movimentoNodoEsquerdo,movimentoNodoDireito]=obj.separarMovimentoPorElemento(movimentos);
            grauDeVibracao=(movimentoNodoEsquerdo+movimentoNodoDireito)/2;
            
            
        end
        
        function [esquerdo,direito]=separarMovimentoPorElemento(obj,movimentos)
            
            totalNos=length(movimentos);
            assert(totalNos>=2);
            
            esquerdo=movimentos(1:(totalNos-1));
            direito=movimentos(2:(totalNos));
            
        end
        
        function infoElementos=gerarInformacoesPorElemento(obj,tipoMovIndependente,movsIndependentes,grauDeVibracao)
            numeroDeElementos=length(grauDeVibracao);
            mapaMovimentosEsq=containers.Map;
            mapaMovimentosDir=containers.Map;
            
            
            for k=1:length(tipoMovIndependente)
                
                [movimentoEsquerdo,movimentoDireito]=obj.separarMovimentoPorElemento(movsIndependentes{k});
                tipoMov=tipoMovIndependente{k};
                mapaMovimentosEsq(tipoMov.char)=movimentoEsquerdo;
                mapaMovimentosDir(tipoMov.char)=movimentoDireito;
                
            end
            
            import lva.programa_oo.modelo.MovimentosIndependentes;
            
            infoElementos=zeros(numeroDeElementos,13);
            mapaColunasMovimentos=containers.Map;
            
            listaMovimentos={...
                MovimentosIndependentes.DESCOLOCAMENTO_X,...
                MovimentosIndependentes.DESCOLOCAMENTO_Y,...
                MovimentosIndependentes.DESCOLOCAMENTO_Z,...
                MovimentosIndependentes.ROTACAO_X,...
                MovimentosIndependentes.ROTACAO_Y,...
                MovimentosIndependentes.ROTACAO_Z};
            
            listaMapas={mapaMovimentosEsq,mapaMovimentosDir};
            
            for i=[1 2]
                
                for k=1:length(listaMovimentos)
                    mapaColunasMovimentos(listaMovimentos{k}.char)=k+(i-1)*6;
                end
                
                mapaNo=listaMapas{i};
                
                for k=1:length(tipoMovIndependente)
                    
                    tipoMov=tipoMovIndependente{k};
                    coluna=mapaColunasMovimentos(tipoMov.char);
                    movimentos=mapaNo(tipoMov.char);
                    infoElementos(:,coluna)=movimentos;
                    
                end
                
            end
            
            infoElementos(:,13)=grauDeVibracao;
            
        end
    end
    
    methods(Access='protected')
        
        [tipoMovIndependente,movsIndependentes,indiceMovCalcGrauDeVibracao]=separarMovimentosIdependentes(obj,movimentosModo);
    end
end

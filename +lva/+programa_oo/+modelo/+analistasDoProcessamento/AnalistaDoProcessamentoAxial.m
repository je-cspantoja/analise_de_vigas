classdef AnalistaDoProcessamentoAxial<lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamento
  
    
        methods(Access='protected')
        
        function [tipoMovIndependente,movsIndependentes,indiceMovCalcGrauDeVibracao]=separarMovimentosIdependentes(obj,movimentosModo)
                      
            import lva.programa_oo.modelo.MovimentosIndependentes;
            indiceMovCalcGrauDeVibracao=1;
            tipoMovIndependente={MovimentosIndependentes.DESCOLOCAMENTO_X};
            movsIndependentes={movimentosModo};
            
            
        end
        
    end
    
end


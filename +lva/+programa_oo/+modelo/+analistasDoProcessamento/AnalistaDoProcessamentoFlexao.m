classdef AnalistaDoProcessamentoFlexao<lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamento
    
    properties(Access='private')
    listaDeMovimentos;
    end
    
    methods
        
        function obj=AnalistaDoProcessamentoFlexao(eixo)
        obj@lva.programa_oo.modelo.analistasDoProcessamento.AnalistaDoProcessamento();
        import lva.programa_oo.modelo.MovimentosIndependentes;
        switch(eixo)
            case {'y','Y'}
              obj.listaDeMovimentos={MovimentosIndependentes.DESCOLOCAMENTO_Y,MovimentosIndependentes.ROTACAO_Z};
       
            case {'z','Z'}
                   obj.listaDeMovimentos={MovimentosIndependentes.DESCOLOCAMENTO_Z,MovimentosIndependentes.ROTACAO_Y};
       
            otherwise
                error('');
        end
        
        end
        
    end
    
        methods(Access='protected')
        
        function [tipoMovIndependente,movsIndependentes,indiceMovCalcGrauDeVibracao]=separarMovimentosIdependentes(obj,movimentosModo)
            
            indiceMovCalcGrauDeVibracao=1;
            movsIndependentes{2}=[];
            
            tipoMovIndependente=obj.listaDeMovimentos;
            
            movsIndependentes{1}=movimentosModo(1:2:length(movimentosModo));
            movsIndependentes{2}=movimentosModo(2:2:length(movimentosModo));
            
        end
        
    end
    
end



classdef (Abstract) Elemento<handle
    
    properties(Access='protected')
        
        id;
        nodos;
        geometria;
        material;
        
    end
    
    
    properties(Constant)
        
        classeNodos='util.Lista';
        
    end
    
    methods
        
        function obj=Elemento(id,nodos,geometria, material)
            assert(isscalar(id),'O argumento id deve ser escalar');
            assert(isnumeric(id),'O argumento id deve ser um numero inteiro');
            assert(isa(nodos,obj.classeNodos),['O argumento nodos deve ser um instancia da classe ' obj.classeNodos]);
            
            obj.geometria=geometria;
            obj.id=id;
            obj.nodos=nodos;
            obj.material= material;
            
        end
        
        
        function dadosMontagem=obterMatrizDeMassa(obj)
            [lin,col]=obj.indicesDaMatrizDePropriendades();
            mat=obj.calcularMatrizDeMassa();
            dadosMontagem=struct('linhas',lin,'colunas',col,'matriz',mat);
        end
        
        function dadosMontagem=obterMatrizDeRigidez(obj)
            [lin,col]=obj.indicesDaMatrizDePropriendades();
            mat=obj.calcularMatrizDeRigidez();
            dadosMontagem=struct('linhas',lin,'colunas',col,'matriz',mat);
        end
        
        
        
    end
    
    
    methods(Access='protected')
        
        function [lin,col]=indicesDaMatrizDePropriendades(obj)
            
            
            it=obj.nodos.iterador();
            
            tamanho=obj.nodos.totalDeItens();
            
            
            blocoDeLinhas=util.Lista(tamanho);
            
            
            
            while it.possuiProximo()
                
                nodo=it.proximo();
                bloco=nodo.indicesDePosicionamento();
                blocoDeLinhas.adicionarItem(bloco);
                
            end
            
            lin= blocoDeLinhas.paraMatriz();
            lin=sort(lin);
            col=lin;
            
        end
        
        
    end
    
    methods(Access='protected')
        
        mMassa=calcularMatrizDeMassa(obj);
        mRigidez=calcularMatrizDeRigidez(obj);
        
        
        
    end
end


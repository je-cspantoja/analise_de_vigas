classdef VigaTorcao<lva.programa_oo.modelo.elementos.Elemento
    
    
    properties(Constant)
        
        constanteMassa=[2/3 1/3;1/3, 2/3];
        constanteRigidez=[  1/2 -1/2; -1/2  1/2];
        
    end
    
    methods
        function obj=VigaTorcao(id,nodos,geometria, material)
            obj@lva.programa_oo.modelo.elementos.Elemento(id,nodos,geometria, material);
        end
        
        
      
        
    end
    
    methods(Access='protected')
        
        
        function mMassa=calcularMatrizDeMassa(obj)
           
            rho=obj.material.obterDensidade();
            geom=obj.geometria;
            a=geom.obterComprimento()/2;
            Ix=geom.obterSegundoMomentoDeAreaEmX();
            mMassa=(rho*a*Ix)*obj.constanteMassa;
            
        end
        
        
        function mRigidez=calcularMatrizDeRigidez(obj)
            
           
            G=obj.material.obterModuloDeCisalhamento();
            geom=obj.geometria;
            J=geom.obterMomentoPolar();
            a=geom.obterComprimento()/2;
            
            mRigidez=((G*J)/a)*obj.constanteRigidez;

            
            
        end
        
    end
end


classdef VigaFlexao<lva.programa_oo.modelo.elementos.Elemento
    
    properties(Access='private')
        obterSegundoMomentoDeArea;
   
    end
    
    methods
        
        function obj=VigaFlexao(id,nodos,geometria, material,eixo)
            obj@lva.programa_oo.modelo.elementos.Elemento(id,nodos,geometria, material); 
            
            switch(eixo)
                case 'z'
                    obj.obterSegundoMomentoDeArea=@geometria.obterSegundoMomentoDeAreaEmZ;
                  
                    
                case 'y'
                    obj.obterSegundoMomentoDeArea=@geometria.obterSegundoMomentoDeAreaEmY;
                  
                    
                otherwise
                    
                    error('O valor argumentado para eixo nao e valido.');
                    
            end
        end
        

        

        
    end
    
    methods(Access='protected')
        
        
        
        function mMassa=calcularMatrizDeMassa(obj)
            
            a=obj.geometria.obterComprimento()/2;
            meiaMassa=obj.geometria.obterMassa(obj.material)/2;
            matriz=zeros(4);
            matriz(1,:)=[ 78   22*a    27      -13*a    ];
            matriz(2,:)=[ 22*a 8*(a^2) 13*a    -6*(a^2)];
            matriz(3,:)=[ 27   13*a    78      -22*a     ];
            matriz(4,:)=[ -13*a -6*(a^2) -22*a 8*(a^2)];
            matriz=matriz/105;
            mMassa=meiaMassa*matriz;
            
        end
        
        
        function mRigidez=calcularMatrizDeRigidez(obj)
            
            a=obj.geometria.obterComprimento()/2;
            E=obj.material.obterModuloDeElasticidade();
            I=obj.obterSegundoMomentoDeArea();
            
            matriz=zeros(4);
            matriz(1,:)=[  3 3*a -3 3*a];
            matriz(2,:)=[3*a 4*(a^2) -3*a 2*(a^2) ];
            matriz(3,:)=[-3 -3*a 3 -3*a];
            matriz(4,:)=[3*a 2*(a^2)  -3*a 4*(a^2)];
            matriz=matriz/2;
            mRigidez=( (E*I)/(a^3) )*matriz;
            
            
        end
        

        
      
        
    end
end


classdef VigaAxial<lva.programa_oo.modelo.elementos.Elemento
    
    properties(Constant)
    
        constanteMassa=[2/3 1/3; 1/3, 2/3];
        constanteRigidez=[  1/2 -1/2;-1/2  1/2];
    end
    
    methods
        
        function obj=VigaAxial(id,nodos,geometria, material)
            obj@lva.programa_oo.modelo.elementos.Elemento(id,nodos,geometria, material);
        end
        
        
      
        
    end
    
    
    
    methods(Access='protected')
        
        
        function mMassa=calcularMatrizDeMassa(obj)        
            meiaMassa=obj.geometria.obterMassa(obj.material)/2;
            mMassa=meiaMassa*obj.constanteMassa;
        end
        
        
        function mRigidez=calcularMatrizDeRigidez(obj)
            
            geom=obj.geometria;
            areaYZ=geom.obterAreaNoPlanoYZ();
            comprimento=geom.obterComprimento();
            modElasticidade=obj.material.obterModuloDeElasticidade();
            meioComprimento=comprimento/2;
            mRigidez=((modElasticidade*areaYZ)/meioComprimento)*obj.constanteRigidez;
        
        end
        

    end
end


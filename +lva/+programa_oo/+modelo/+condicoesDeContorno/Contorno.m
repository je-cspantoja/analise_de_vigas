classdef Contorno<handle
    
    properties(Access='private')
    mascaraDeRestricao;
    end
    
    methods
        
        function obj=Contorno(mascaraDeRestricao)
        obj.mascaraDeRestricao=mascaraDeRestricao;
        end
        
        function masc=obterMascaraDeRestricao(obj)
            masc=obj.mascaraDeRestricao;
        
        end
    end
    
end


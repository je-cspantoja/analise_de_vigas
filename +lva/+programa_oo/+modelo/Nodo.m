classdef Nodo<handle
    
    properties(Access='private')

        
        id;
        coordenadas;
        grauDeLiberdade;
        mapaDeMovimentos;
        
    end
    
   
    
    methods
        
        function obj=Nodo(id,coordenadas,movimentosNodais)
            obj.id=id;
            obj.coordenadas=coordenadas;
            obj.grauDeLiberdade=length(movimentosNodais);
            obj.mapaDeMovimentos=containers.Map;
            
            posicoes=obj.calcularPosicoes();
            
            for i=1:obj.grauDeLiberdade
                movNodal=movimentosNodais{i};
                obj.mapaDeMovimentos(movNodal.char)=struct('movimentoIndependente',movNodal,'posicao',posicoes(i),'valor',0,'restrito',false);
            end
        end
        
        
        
        function v=indicesDePosicionamento(obj)
            chaves=obj.mapaDeMovimentos.keys;
            v=zeros(1,obj.grauDeLiberdade);
            
            for i=1:obj.grauDeLiberdade
                infoMov=obj.mapaDeMovimentos(chaves{i});
                v(i)=infoMov.posicao;
            end
            
        end
        
        function valor=obterValorDoMovimento(obj,mov)
            
            try
                
                infoMov=obj.mapaDeMovimentos(mov.char);
                valor=infoMov.valor;
                
            catch erro
                valor=0;
                
            end
            
            
        end
        
        function dadosRestricao=obterDadosDeContorno(obj)
        dadosRestricao=obj.mapaDeMovimentos.values;
        
%         for i=1:length(dadosRestricao) 
%          dadosRestricao{i}.restrito=~dadosRestricao{i}.restrito;   
%         end
        
        end
            
        function aplicarContorno(obj,contorno)
            mascaraDeRestricao=contorno.obterMascaraDeRestricao();
            
            for i=1:obj.grauDeLiberdade
                
                try
                    infoRestricao=mascaraDeRestricao{i};
                    infoMov=obj.mapaDeMovimentos(infoRestricao.movimentoIndependente.char);
                    infoMov.restrito=infoRestricao.restrito;
                    obj.mapaDeMovimentos(infoRestricao.movimentoIndependente.char)=infoMov;
                    
                catch erro
                    
                end
                
            end
            
            
        end
        
        
        function fixarMovimentos(obj,vetor)
        movs=obj.mapaDeMovimentos.values;
        
        for i=1:length(movs)
        info=movs{i};
        info.valor=vetor(info.posicao);
        obj.mapaDeMovimentos(info.movimentoIndependente.char)=info;
        
        end
        
        end
        
        function v=vetorDeRestricao(obj)
            chaves=obj.mapaDeMovimentos.keys;
            v{obj.grauDeLiberdade}=[];
            
            for i=1:obj.grauDeLiberdade
                
                infoMov=obj.mapaDeMovimentos(chaves{i});
                infoRestricao.posicao=infoMov.posicao;
                infoRestricao.valor=~infoMov.restrito;
                v{i}=infoRestricao;
            end
            
            
        end
    end
    
    methods(Access='private')
        function posicoes=calcularPosicoes(obj)
            a1=1;
            n=obj.id;
            razao=obj.grauDeLiberdade;
            an=progressaoAritmetica(a1,n,razao);
            posicoes=an:(an+razao-1);
            
        end
    end
    
    
end


function an=progressaoAritmetica(a1,n,razao)
an=a1+(n-1)*razao;
end
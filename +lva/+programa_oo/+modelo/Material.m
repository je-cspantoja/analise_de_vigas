classdef Material<handle
   
    
    properties(Access='private')
    nome;
    densidade;
    modElasticidade;
    coefPoisson;
    corRgb;
    
    end
    
    methods
        
        function obj=Material(nome,densidade,modElasticidade,coefPoisson,corRgb)
            
            obj.nome=nome;
            obj.densidade=densidade;
            obj.modElasticidade=modElasticidade;
            obj.coefPoisson=coefPoisson;
            obj.corRgb=corRgb;
            
        
        end
        
        function E=obterModuloDeElasticidade(obj)
        E=obj.modElasticidade;
        
        end
        
        function rho=obterDensidade(obj)
        rho=obj.densidade;
        end
        
        function G=obterModuloDeCisalhamento(obj)
            G=obj.modElasticidade/(2*(1+obj.coefPoisson));
        end
        
        
        
    end
    
        
end





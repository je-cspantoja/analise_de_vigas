classdef RepositorioDeMaterial<handle
    
    
    properties(Access='private')
        mapaDeMateriais;
    end
    
    methods
        function obj=RepositorioDeMaterial()
            obj.mapaDeMateriais=criarMapaDeMateriais();
        end
        
        function mats=obterListaDeMateriais(obj)
            mats=obj.mapaDeMateriais.keys;
        end
        
        function material=obterMaterial(obj,nomeMaterial)
            
            material=obj.mapaDeMateriais(nomeMaterial);
            
        end
    end
    
    
    
    
   
    
end

   function mapa=criarMapaDeMateriais()
            import lva.programa_oo.modelo.Material;
            
            mapa=containers.Map;
            mapa('Aço')=Material('Aço',7700,200e9,0.29,[0.5 0.5 0.5]);
            mapa('Alumínio')=Material('Alumínio',2700,71e9,0.33,[0.8125 0.8125 0.8125]);
            mapa('Ferro')=Material('Ferro',7874,211e9,0.29,[0.5625 0.5625 0.5625]);
            mapa('Estanho')=Material('Estanho',7300,45e9,0.33,[0.75 0.75 0.625]);
            mapa('Zinco')=Material('Zinco',7100,108e9,0.17,[0.75 0.75 0.75]);
            mapa('Cobre')=Material('Cobre',8900,122e9,0.35,[0.625 0.25 0.0625]);
            mapa('Chumbo')=Material('Chumbo',11340,16e9,0.44,[0.375 0.375 0.4375]);
            mapa('Bronze')=Material('Bronze',8800,118e9,0.34,[0.625 0.4375 0.125]);
            mapa('Latão')=Material('Latão',8850,112e9,0.34,[0.6875 0.4375 0.125]);
            mapa('Prata')=Material('Prata',10490,83e9,0.37,[0.6875 0.6875 0.6875]);
            mapa('Ouro')=Material('Ouro',19300,79e9,0.44,[0.875 0.6875 0.1875]);
            mapa('Platina')=Material('Platina',21450,168e9,0.38,[0.75 0.75 0.8125]);
            mapa('Níquel')=Material('Níquel',98908,200e9,0.31,[0.8125 0.75 0.5]);
            mapa('Titânio')=Material('Titânio',4506,116e9,0.32,[0.625 0.625 0.625]);
            mapa('Tungstênio')=Material('Tungstênio',19250,411e9,0.28,[0.75 0.75 0.75]);
            mapa('Borracha')=Material('Borracha',1200,0.05e9,0.5,[ 0.25 0.25 0.25]);
            mapa('Vidro')=Material('Vidro',2600,70e9,0.22,[0.9375 0.9375 0.9375]);
            mapa('Diamante')=Material('Diamante',3510,1220e9,0.1,[1 1 1]);
            mapa('Safira')=Material('Safira',3980,435e9,0.27,[0 0  0.75]);
            mapa('Rubi')=Material('Rubi',3980,435e9,0.27,[0.75 0 0]);
            
            
        end

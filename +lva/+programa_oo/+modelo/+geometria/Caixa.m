classdef Caixa<lva.programa_oo.modelo.geometria.FormaGeometrica
    
    
    properties(Access='private')
        
        origem;
        dimensao;
        
    end
    
    methods
        
        
        function obj=Caixa(origem,dimensao)
            
            obj.origem=origem;
            obj.dimensao=dimensao;
            
        end
        
        function l=obterLimites(obj)
            l.origem=obj.origem;
            l.dimensao=obj.dimensao;
        end
        
        function I=obterSegundoMomentoDeAreaEmX(obj)
            
            Iz=obj.obterSegundoMomentoDeAreaEmZ();
            Iy=obj.obterSegundoMomentoDeAreaEmY();
            I=Iy+Iz;
        end
        
        function I=obterSegundoMomentoDeAreaEmY(obj)
            
            Hy=obj.dimensao.y;
            Bz=obj.dimensao.z;
            I=Bz*(Hy^3)/12;
        end
        
        function I=obterSegundoMomentoDeAreaEmZ(obj)
            Hy=obj.dimensao.y;
            Bz=obj.dimensao.z;
            I=Hy*(Bz^3)/12;
            
        end
        
        function m=obterMassa(obj,material)
            volume=obj.obterComprimento()*obj.obterAreaNoPlanoYZ();
            m=volume*material.obterDensidade();
        end
        
        function c=obterComprimento(obj)
            c=obj.dimensao.x;
        end
        
        function Ayz=obterAreaNoPlanoYZ(obj)
            Ayz=obj.dimensao.y*obj.dimensao.z;
        end
        
        function J=obterMomentoPolar(obj)
            
            Hy=obj.dimensao.y;
            Bz=obj.dimensao.z;
            
            coef1 = ((1/3)*((Hy^3)*(Bz)));
            coef2 = ((Hy*192)./(Bz*(pi^5)));
            coef3 = 0;
            
            
            
            for k=1:10000
                
                
                coef3 = coef3 + tanh((pi/2)*((2*k)-1)*(Bz/Hy))/(((2*k)-1)^5);
            end
            
            J = coef1*(1-coef2*coef3);
            
        end
        
    end
    
    
    
end


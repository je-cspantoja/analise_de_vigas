classdef (Abstract) FormaGeometrica<handle
   
   
    
    methods
    
    Ix=obterSegundoMomentoDeAreaEmX(obj);
    Iz=obterSegundoMomentoDeAreaEmY(obj);
    Iy=obterSegundoMomentoDeAreaEmZ(obj);
    m=obterMassa(obj,material);
    c=obterComprimento(obj);
    Ayz=obterAreaNoPlanoYZ(obj);
    J=obterMomentoPolar(obj);
    
    end
    
end


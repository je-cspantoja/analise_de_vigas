classdef Quadro3D<gui.containers.Eixos
    
   
    methods
        
        function obj=Quadro3D(pai)
            obj@gui.containers.Eixos(pai);
            id=obj.obterId();
            axes(id);
            view(-30,30);
            

          
        end
        
        function geoms3d=converterEmFiguras3D(obj,geoms)
            it=geoms.iterador();
            geoms3d=util.Lista(geoms.totalDeItens());
            
            while it.possuiProximo()
                g=it.proximo();
                limites=g.obterLimites();
                g3d=formas3D.Caixa(limites.origem,limites.dimensao);
                geoms3d.adicionarItem(g3d);
            end
            
            
            
            
        end
        
        function desenharFiguras3D(obj,figuras3D)
             
            axes(obj.obterId());
            cla;
            zoom out;
            it=figuras3D.iterador();
            
            while it.possuiProximo()
                g3d=it.proximo();
                infoPatch=g3d.obterParametrosDeIlustracao();
                patch(infoPatch);
                hold on;
                
            end
            
            axis equal;
            
            axis(axis);
            
            light;
            set(gca,'CameraViewAngleMode','Manual');
            
            
        end
    end
    
end


classdef PainelEntradaESaida<gui.containers.Painel
    
    
    properties(Access='private')
        nomeArquivoPreProcessamento;
        nomeArquivoProcessamento;
        
        painelArquivoPreproc;
        painelArquivoProc;
        
    end
    
    events
        
        modificacaoArqEntrada;
        modificacaoArqSaida;
        
    end
    
    methods
        
        function obj=PainelEntradaESaida(pai)
            obj@gui.containers.Painel(pai,'');
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.dimensionarComponentes();
            
        end
        
        function endereco=obterEnderecoArquivoDeEntrada(obj)
            endereco=obj.nomeArquivoPreProcessamento;
        end
        
        
        function endereco=obterEnderecoArquivoDeSaida(obj)
            endereco=obj.nomeArquivoProcessamento;
        end
    end
    
    
    methods(Access='private')
        function criarComponentes(obj)
            
            
            obj.painelArquivoPreproc=lva.programa_oo.visao.PainelEntradaDeDados(obj,'Arquivo de pre-processamento');
            obj.painelArquivoProc=lva.programa_oo.visao.PainelSaidaDeDados(obj,'Arquivo do processamento');
                    
            
%             obj.painelArquivoPreproc;
%             obj.painelArquivoProc;
            
            
            obj.painelArquivoPreproc.addlistener('modificacaoNomeDoArquivo',@obj.editarEnderecoArquivoDeEntrada);
            obj.painelArquivoProc.addlistener('modificacaoNomeDoArquivo',@obj.editarEnderecoArquivoDeSaida);
            
   
            
        end
        
        
        function posicionarComponentes(obj)
            
            
            obj.painelArquivoPreproc.fixarPosicao(2,49);
            obj.painelArquivoProc.fixarPosicao(2,12);
            
            
        end
        
        function dimensionarComponentes(obj)
            obj.fixarDimensao(700,100);
                     
            obj.painelArquivoPreproc.fixarDimensao(690,50);
            obj.painelArquivoProc.fixarDimensao(690,50);
        
        end
        
        function editarEnderecoArquivoDeEntrada(obj,src,evt)   
            obj.nomeArquivoPreProcessamento=src.obterNomeDoArquivo();
            obj.notify('modificacaoArqEntrada');
       
        
        
        end
        
        function editarEnderecoArquivoDeSaida(obj,src,evt)
         obj.nomeArquivoProcessamento=src.obterNomeDoArquivo();   
         obj.notify('modificacaoArqSaida');
        end
        
        
        
    end
end


classdef JanelaProcessamento<gui.containers.Janela
    
    properties(Access='private')
        
        
        
        painelConfigMovimento;
        painelArquivos;
        quadro3d;
        repositorioMaterial;
        processamento;
        mapaMovimentos;
        mapaCondicaoDeContorno;
        
    end
    
    methods
        
        function obj=JanelaProcessamento()
            
            obj@gui.containers.Janela('Processamento');
            tamanhoTela=get(0,'ScreenSize');
            obj.habilitarBarraDeFerramentasDeFigura(true);
            obj.fixarDimensao(tamanhoTela(3),tamanhoTela(4)-80);
            obj.criarComponentes();
            obj.configuraValoresIniciais();
            obj.posicionarComponentes();
            obj.configurarOuvintesDeEventos();
            obj.fixarVisivel(true);
        end
        
        
        
    end
    
    methods(Access='private')
        function criarComponentes(obj)
            import lva.programa_oo.visao.processamento.PainelConfiguracoesMovimento;
            import lva.programa_oo.visao.Quadro3D;
            import lva.programa_oo.visao.processamento.PainelEntradaESaida;
            
            
            
            obj.painelArquivos=PainelEntradaESaida(obj);
            obj.painelConfigMovimento=PainelConfiguracoesMovimento(obj);
            obj.quadro3d=Quadro3D(obj);
            obj.quadro3d.eVisivel(false);
            
            
        end
        
        function configuraValoresIniciais(obj)
          
            obj.mapaMovimentos=containers.Map;
            import lva.programa_oo.modelo.movimentos.TiposDeMovimento;
            import lva.programa_oo.modelo.fachada.Processamento;
            obj.repositorioMaterial=lva.programa_oo.modelo.RepositorioDeMaterial();
            
            obj.mapaMovimentos(TiposDeMovimento.AXIAL.char)=TiposDeMovimento.AXIAL;
            obj.mapaMovimentos(TiposDeMovimento.TORCAO.char)=TiposDeMovimento.TORCAO;
            obj.mapaMovimentos(TiposDeMovimento.FLEXAO_EM_Y.char)=TiposDeMovimento.FLEXAO_EM_Y;
            obj.mapaMovimentos(TiposDeMovimento.FLEXAO_EM_Z.char)=TiposDeMovimento.FLEXAO_EM_Z;
            
            
            listaMateriais=obj.repositorioMaterial.obterListaDeMateriais();
            obj.painelConfigMovimento.fixarOpcoesDeMovimento(obj.mapaMovimentos.keys);
            obj.painelConfigMovimento.fixarOpcoesDeMaterial(listaMateriais);
            moviInicial=obj.painelConfigMovimento.obterTipoDeMovimento();
            moviInicial=obj.mapaMovimentos(moviInicial);
            opcoesDeContorno=moviInicial.obterOpcoesDeContorno();
            
            
            obj.mapaCondicaoDeContorno=containers.Map;
            
            for i=1:length(opcoesDeContorno)
                obj.mapaCondicaoDeContorno(opcoesDeContorno{i}.char)=opcoesDeContorno{i};
            end
            
            
            obj.painelConfigMovimento.fixarOpcoesDeContorno(obj.mapaCondicaoDeContorno.keys);
            
            
            contEsq=obj.painelConfigMovimento.obterContornoFaceEsquerda();
            contEsq=obj.mapaCondicaoDeContorno(contEsq);
            contDir=obj.painelConfigMovimento.obterContornoFaceDireita();
            contDir=obj.mapaCondicaoDeContorno(contDir);
            nomeMaterial=obj.painelConfigMovimento.obterMaterial();
            material=obj.repositorioMaterial.obterMaterial(nomeMaterial);
            obj.processamento=Processamento(moviInicial,contEsq,contDir,material);
            
            
            
        end
        
        function posicionarComponentes(obj)
            
            obj.fixarPosicao(1,30);
            [x y]=obj.obterDimensao();
            obj.painelConfigMovimento.fixarPosicao(1,y-375);
            obj.painelArquivos.fixarPosicao(1,y-100);
            obj.quadro3d.fixarPosicao(169,5);
            obj.quadro3d.fixarDimensao(x-169,y-100);
            
        end
        
        function configurarOuvintesDeEventos(obj)
            
            obj.painelArquivos.addlistener('modificacaoArqEntrada',@obj.fixarArquivoDePreProcessamento);
            obj.painelArquivos.addlistener('modificacaoArqSaida',@obj.fixarArquivoDeProcessamento);
            
            obj.painelConfigMovimento.addlistener('modificacaoTipoDeMovimento',@obj.fixarTipoDeMovimento);
            obj.painelConfigMovimento.addlistener('modificacaoMaterial',@obj.fixarMaterial);
            obj.painelConfigMovimento.addlistener('modificacaoContornoEsq',@obj.fixarContornoFaceEsquerda);
            obj.painelConfigMovimento.addlistener('modificacaoContornoDir',@obj.fixarContornoFaceDireita);
            obj.painelConfigMovimento.addlistener('cliqueBotaoProcessar',@obj.acaoProcessar);
            
            
        end
        
        function acaoProcessar(obj,src,evt)
            try
                obj.processamento.processar();
                 javax.swing.JOptionPane.showMessageDialog([],'Processamento realizado com sucesso.');
            catch e
                javax.swing.JOptionPane.showMessageDialog([],e.message);
            end
            
        end
        
        
        function fixarArquivoDePreProcessamento(obj,src,evt)
            
            try
                
                endereco=obj.painelArquivos.obterEnderecoArquivoDeEntrada();
                geoms=obj.processamento.carregarArquivoDePreProcessamento(endereco);
                geoms3d=obj.quadro3d.converterEmFiguras3D(geoms);
                obj.quadro3d.desenharFiguras3D(geoms3d);
                
                javax.swing.JOptionPane.showMessageDialog([],'Arquivo pre-processamento carregado com sucesso');
                
            catch e
                javax.swing.JOptionPane.showMessageDialog([],e.message);
            end
            
            
            
        
        end
        
        function fixarArquivoDeProcessamento(obj,src,evt)
            endereco=obj.painelArquivos.obterEnderecoArquivoDeSaida();
            obj.processamento.fixarArquivoDeProcessamento(endereco);
        end
        
        function fixarMaterial(obj,src,evt)
            nomeMaterial=obj.painelConfigMovimento.obterMaterial();
            material=obj.repositorioMaterial.obterMaterial(nomeMaterial);
            obj.processamento.fixarMaterial(material);
        end
        
        function fixarTipoDeMovimento(obj,src,evt)
            tipoMov=obj.painelConfigMovimento.obterTipoDeMovimento();
            tipoMov=obj.mapaMovimentos(tipoMov);
            opcoesDeContorno=tipoMov.obterOpcoesDeContorno();
            obj.mapaCondicaoDeContorno=containers.Map;
            
            for i=1:length(opcoesDeContorno)
                obj.mapaCondicaoDeContorno(opcoesDeContorno{i}.char)=opcoesDeContorno{i};
            end
            
            
            obj.processamento.fixarTipoDeMovimento(tipoMov);
            obj.painelConfigMovimento.fixarOpcoesDeContorno(obj.mapaCondicaoDeContorno.keys);
            
            
        end
        
        function fixarContornoFaceEsquerda(obj,src,evt)
            contorno=obj.painelConfigMovimento.obterContornoFaceEsquerda();
            contorno=obj.mapaCondicaoDeContorno(contorno);
            obj.processamento.fixarContornoFaceEsquerda(contorno);
        end
        
        function fixarContornoFaceDireita(obj,src,evt)
            contorno=obj.painelConfigMovimento.obterContornoFaceDireita();
            contorno=obj.mapaCondicaoDeContorno(contorno);
            obj.processamento.fixarContornoFaceDireita(contorno);
        end
        
        
        
        
    end
    
end


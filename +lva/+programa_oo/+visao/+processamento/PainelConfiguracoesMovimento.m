classdef PainelConfiguracoesMovimento<gui.containers.Painel
    
    properties(Access='private')
        
        painelContorno;
        cxSuspContornoEsq;
        cxSuspContornoDir;
        cxSuspMaterial;
        cxSuspTipoMovimento;
        rotuloFaceEsq;
        rotuloFaceDir;
        rotuloTipoMovimento;
        rotuloMaterial;
        btProcessar;
        
        
    end
    
    events
    modificacaoTipoDeMovimento;
    modificacaoMaterial;
    modificacaoContornoEsq;
    modificacaoContornoDir;
    cliqueBotaoProcessar;
    end
    
    methods
        
        function obj=PainelConfiguracoesMovimento(pai)
            obj@gui.containers.Painel(pai,'Configuracoes Do Movimento');
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.dimensionarComponentes();
            
        end
        
        
        function fixarOpcoesDeMovimento(obj,opcs)
            obj.cxSuspTipoMovimento.fixarTexto(opcs);
        end
        
        function fixarOpcoesDeMaterial(obj,opcs)
            obj.cxSuspMaterial.fixarTexto(opcs);
        end
        
        
        function fixarOpcoesDeContorno(obj,opcs)
            obj.cxSuspContornoEsq.fixarTexto(opcs);
            obj.cxSuspContornoDir.fixarTexto(opcs);
        end
        
        
        
        function mov=obterTipoDeMovimento(obj)
            mov=obj.cxSuspTipoMovimento.obterItemSelecionado();
        end
        
        function m=obterMaterial(obj)
            m=obj.cxSuspMaterial.obterItemSelecionado();
        end
        
        function c=obterContornoFaceEsquerda(obj)
            c= obj.cxSuspContornoEsq.obterItemSelecionado();
        end
        function c=obterContornoFaceDireita(obj)
            c= obj.cxSuspContornoDir.obterItemSelecionado();
        end
        
        
    end
    
    methods(Access='private')
        
        
        function criarComponentes(obj)
            
            import gui.componentes.CaixaSuspensa;
            import gui.containers.Painel;
            import gui.componentes.Rotulo;
            
            import gui.componentes.Botao;
            
            obj.btProcessar=Botao(obj);
            obj.btProcessar.fixarTexto('processar');
            obj.painelContorno=Painel(obj,'Contorno');
            obj.cxSuspContornoEsq=CaixaSuspensa(obj.painelContorno);
            obj.cxSuspContornoDir=CaixaSuspensa(obj.painelContorno);
            obj.rotuloFaceEsq=Rotulo(obj.painelContorno);
            obj.rotuloFaceDir=Rotulo(obj.painelContorno);
            obj.rotuloTipoMovimento=Rotulo(obj);
            obj.rotuloMaterial=Rotulo(obj);
            
            obj.cxSuspMaterial=CaixaSuspensa(obj);
            obj.cxSuspTipoMovimento=CaixaSuspensa(obj);
            
            
            obj.rotuloFaceEsq.fixarTexto('Face Esquerda');
            obj.rotuloFaceDir.fixarTexto('Face Direita');
            obj.rotuloTipoMovimento.fixarTexto('Tipo Movimento');
            obj.rotuloMaterial.fixarTexto('Material');
            
            obj.cxSuspTipoMovimento.addlistener('mudancaDeItemSelecionado',@(src,evt)obj.notify('modificacaoTipoDeMovimento'));
            obj.cxSuspContornoEsq.addlistener('mudancaDeItemSelecionado',@(src,evt)obj.notify('modificacaoContornoEsq'));
            obj.cxSuspContornoDir.addlistener('mudancaDeItemSelecionado',@(src,evt)obj.notify('modificacaoContornoDir'));
            obj.cxSuspMaterial.addlistener('mudancaDeItemSelecionado',@(src,evt)obj.notify('modificacaoMaterial'));
            obj.btProcessar.addlistener('componenteClicado',@(src,evt)obj.notify('cliqueBotaoProcessar'));
            
       
            
        end
        
        function dimensionarComponentes(obj)
            
            obj.fixarDimensao(165,270);
            obj.painelContorno.fixarDimensao(150,110);
            obj.cxSuspContornoEsq.fixarDimensao(115,20);
            obj.cxSuspContornoDir.fixarDimensao(115,20);
            
            obj.cxSuspMaterial.fixarDimensao(115,20);
            obj.cxSuspTipoMovimento.fixarDimensao(115,20);
            
            obj.rotuloFaceEsq.fixarDimensao(115,15);
            obj.rotuloFaceDir.fixarDimensao(115,15);
            obj.rotuloTipoMovimento.fixarDimensao(115,15);
            obj.rotuloMaterial.fixarDimensao(115,15);
             obj.btProcessar.fixarDimensao(100,18);
            
            
        end
        
        function posicionarComponentes(obj)
            obj.btProcessar.fixarPosicao(30,5);
            
            obj.painelContorno.fixarPosicao(5,35);
            
            obj.rotuloFaceEsq.fixarPosicao(13,97);
            obj.rotuloFaceDir.fixarPosicao(13,55);
            obj.cxSuspContornoEsq.fixarPosicao(13,76);
            obj.cxSuspContornoDir.fixarPosicao(13,34);
            
            
            obj.rotuloTipoMovimento.fixarPosicao(20,226);
            obj.rotuloMaterial.fixarPosicao(20,187);
            obj.cxSuspMaterial.fixarPosicao(20,167);
            obj.cxSuspTipoMovimento.fixarPosicao(20,205);
            
        end
    end
    
end


classdef Teste<gui.containers.Janela
    
    properties
        quadro;
        escalaVisualizacaoDeslocamentos;
        escalaVisualizacaoRotacoes;
        geoms3d;
        
    end
    
    methods
        function obj=Teste
            obj@gui.containers.Janela('');
            
            obj.habilitarBarraDeFerramentasDeFigura(true);
            obj.quadro=lva.programa_oo.visao.Quadro3D(obj);
            obj.fixarPosicao(1,30);
            tamanhoTela=get(0,'ScreenSize');
            obj.fixarDimensao(tamanhoTela(3),tamanhoTela(4)-80);
            
            obj.fixarVisivel(true);
            obj.seila();
        end
        
        function seila(obj)
            
            dados=load('axial.mat');
            
            geometria=dados.solucao.geometria;
            obj.geoms3d=obj.quadro.converterEmFiguras3D(geometria);
            tipoMovimento=dados.solucao.tipoMovimento;
            modos=tipoMovimento.gerarModosDeVibracao(dados.solucao);
            comprimentoDaEstrutura=dados.solucao.comprimentoDaEstrutura;
            obj.escalaVisualizacaoDeslocamentos=comprimentoDaEstrutura/30;
            obj.escalaVisualizacaoRotacoes=deg2rad(45);
            
            while true
            numModo=input('digite o numero do modo: ');
            modo=modos{numModo};
            
            geom3dTransf=obj.aplicarTransformacoesGeometricas(modo);
            
            obj.quadro.desenharFiguras3D(geom3dTransf);
            
            xlabel('eixo x');
            ylabel('eixo y');
            zlabel('eixo z');  
            end
          
        end
        
        function geoms3dTransf=aplicarTransformacoesGeometricas(obj,modo)
            
            numeroDeElementos=obj.geoms3d.totalDeItens();
            geoms3dTransf=util.Lista(numeroDeElementos);
            
            for i=1:numeroDeElementos
                
                infoEl=modo.infoElementos(i,:);
                g=obj.geoms3d.item(i).copia();
                
                g.moverFace(g.esquerda,obj.escalaVisualizacaoDeslocamentos*infoEl([1 2 3]));
                g.rotacionarFace(g.esquerda,obj.escalaVisualizacaoRotacoes*infoEl([4 5 6]));
                g.moverFace(g.direita, obj.escalaVisualizacaoDeslocamentos*infoEl([7 8 9]));
                g.rotacionarFace(g.direita,obj.escalaVisualizacaoRotacoes*infoEl([10 11 12]));
                g.fixarCor(determinarCor(infoEl(13)));
                geoms3dTransf.adicionarItem(g);
                
            end
            
            
        end
        
    end
    
    
end





function cor=determinarCor(valor)
valor=abs(valor);

if valor<0.5
    r=0;
    g=valor/0.5;
    b=1;
    cor=[r g b];
    
    return;
end

r=1;
g=1-((valor-0.5)/0.5);
b=0;

cor=[r g b];
end

classdef (Abstract)PainelArquivo<gui.containers.Painel
 
   
    properties(Access='private')
        
        cxTxtNomeArquivo;
        btEditarArquivo;
        
    end
    
    events
        
    modificacaoNomeDoArquivo;
    
    end
    
    methods
        
        function obj=PainelArquivo(pai,titulo)
   
            obj@gui.containers.Painel(pai,titulo);
            obj.criarComponentes();
            
        end
        
        function endereco=obterNomeDoArquivo(obj)
            endereco=obj.cxTxtNomeArquivo.obterTexto();
        end
        
        function fixarDimensao(obj,x,y)
         fixarDimensao@gui.containers.Painel(obj,x,y);
         obj.cxTxtNomeArquivo.fixarDimensao(x-90,18);
         obj.btEditarArquivo.fixarDimensao(80,18);
         obj.cxTxtNomeArquivo.fixarPosicao(2,12);
         obj.btEditarArquivo.fixarPosicao(x-85,12);
            
        end
    end
    
    
    methods(Access='private')

        function criarComponentes(obj)
            
            import gui.componentes.CaixaDeTexto;
            import gui.componentes.Botao;
                   
            obj.cxTxtNomeArquivo=CaixaDeTexto(obj);
            obj.cxTxtNomeArquivo.fixarHabilitar(false);
            obj.btEditarArquivo=Botao(obj);
            obj.btEditarArquivo.addlistener('componenteClicado',@obj.editarNomeDoArquivo);
            obj.btEditarArquivo.fixarTexto('editar');
            
            
        end
        
        
        
       
        
        function editarNomeDoArquivo(obj,src,evt)
        nomeArquivo=obj.mostrarCaixaDeDialogo();
            
            if ~ischar(nomeArquivo)
                return;
            end
            
            enderecoAtual=obj.cxTxtNomeArquivo.obterTexto();
            
            if strcmp(enderecoAtual,nomeArquivo)
                return;
            end
            
            obj.cxTxtNomeArquivo.fixarTexto(nomeArquivo);
            obj.notify('modificacaoNomeDoArquivo');
            
        end
        
        
        
    end
    
    methods(Access='protected')
        nomeCompleto=mostrarCaixaDeDialogo(obj);
    end
end

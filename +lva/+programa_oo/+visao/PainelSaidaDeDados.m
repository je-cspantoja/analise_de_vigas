classdef PainelSaidaDeDados<lva.programa_oo.visao.PainelArquivo
      
 
    
    methods
        
        function obj=PainelSaidaDeDados(pai,titulo)
            obj@lva.programa_oo.visao.PainelArquivo(pai,titulo);
            obj.fixarDimensao(300,50);
            
        end
        
         
        
    end
    methods(Access='protected')
        function nomeCompleto=mostrarCaixaDeDialogo(obj)
            [filename, pathname] = uiputfile('*.mat', '');
            nomeCompleto=[pathname filename];
            
        end
    end
end
classdef JanelaPosProcessamento<gui.containers.Janela
    
    
    properties(Access='private')
        
        entradaDeDados;
        quadro3D;
        posProcessamento;
        geom3DBase;
        escalaVisualizacaoDeslocamentos;
        escalaVisualizacaoRotacoes;
        cxSupensa;
        
    end
    
    methods
        function obj=JanelaPosProcessamento()
            obj@gui.containers.Janela('Pos processamento');
            obj.fixarPosicao(1,30);
            tamanhoTela=get(0,'ScreenSize');
            obj.habilitarBarraDeFerramentasDeFigura(true);
            obj.fixarDimensao(tamanhoTela(3),tamanhoTela(4)-80);
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.adicionarAcoesAosComponentes();
            obj.fixarVisivel(true);
            
            
        end
        
    end
    
    methods(Access='private')
        
        
        function criarComponentes(obj)
            
            
            import gui.componentes.Botao;
            import gui.containers.Painel;
            import gui.componentes.CaixaDeListagem;
            import lva.programa_oo.visao.PainelEntradaDeDados;
            import lva.programa_oo.visao.Quadro3D;
            
            
            obj.entradaDeDados=PainelEntradaDeDados(obj,'Arquivo processamento');
            obj.quadro3D=Quadro3D(obj);
            obj.quadro3D.eVisivel(false);
            obj.cxSupensa=gui.componentes.CaixaSuspensa(obj);
            obj.cxSupensa.fixarTexto({''});
            obj.entradaDeDados.fixarDimensao(660,50);
            
            [x y]=obj.obterDimensao();
            obj.quadro3D.fixarDimensao(x-5,y-170);
            obj.cxSupensa.fixarDimensao(660,20);
           
             
             obj.posProcessamento=lva.programa_oo.modelo.fachada.PosProcessamento();
        end
        
        
        function fixarArquivoDeEntrada(obj,src,evt)
            try
                
                [obj.escalaVisualizacaoDeslocamentos,obj.escalaVisualizacaoRotacoes]=obj.posProcessamento.carregarArquivoProcessamento(src.obterNomeDoArquivo());
                geometria=obj.posProcessamento.obterGeometria();
                obj.geom3DBase=obj.quadro3D.converterEmFiguras3D(geometria);
                
               infoModos=obj.posProcessamento.obterListaDeFrequencias();  
                obj.cxSupensa.fixarTexto(infoModos);
                
                modo=obj.posProcessamento.obterModo(1);
                obj.visualizarModo(modo);
                
                javax.swing.JOptionPane.showMessageDialog([],'Arquivo de processamento carregado com sucesso');
                
            catch e
                
                javax.swing.JOptionPane.showMessageDialog([],e.message);
                
            end
            
          
        end
        
        
        function visualizarModo(obj,modo)
        geom3dTranf=obj.aplicarTransformacoesGeometricas(modo,obj.geom3DBase,obj.escalaVisualizacaoDeslocamentos,obj.escalaVisualizacaoRotacoes);
        obj.desenhar(geom3dTranf);
        end
        
        function desenhar(obj,geoms3d)
        obj.quadro3D.desenharFiguras3D(geoms3d);
        end
        
       
        
        function posicionarComponentes(obj)
            [x,y]=obj.obterDimensao();
            
            obj.entradaDeDados.fixarPosicao(5,y-50);
            obj.quadro3D.fixarPosicao(5,135); 
            obj.cxSupensa.fixarPosicao(5,y-75);
            
        end
        
        function adicionarAcoesAosComponentes(obj)
            
            obj.entradaDeDados.addlistener('modificacaoNomeDoArquivo',@obj.fixarArquivoDeEntrada);
            obj.cxSupensa.addlistener('mudancaDeItemSelecionado',@obj.atualizarModo);  
        
        end
        
        function atualizarModo(obj,src,evt)
           modo=obj.posProcessamento.obterModo(src.obterValor());
           obj.visualizarModo(modo);
        end
        
        
        
        
        function geoms3dTransf=aplicarTransformacoesGeometricas(obj,modo,geoms3d,escalaVisualizacaoDeslocamentos,escalaVisualizacaoRotacoes)
            
            numeroDeElementos=geoms3d.totalDeItens();
            geoms3dTransf=util.Lista(numeroDeElementos);
            
            for i=1:numeroDeElementos
                
                infoEl=modo.infoElementos(i,:);
                g=geoms3d.item(i).copia();
                
                g.moverFace(g.esquerda,escalaVisualizacaoDeslocamentos*infoEl([1 2 3]));
                g.rotacionarFace(g.esquerda,escalaVisualizacaoRotacoes*infoEl([4 5 6]));
                g.moverFace(g.direita,escalaVisualizacaoDeslocamentos*infoEl([7 8 9]));
                g.rotacionarFace(g.direita,escalaVisualizacaoRotacoes*infoEl([10 11 12]));
                g.fixarCor(determinarCor(infoEl(13)));
                geoms3dTransf.adicionarItem(g);
                
            end
            
            
        end
        
    end
    
    
end





function cor=determinarCor(valor)
valor=abs(valor);

if valor<0.5
    r=0;
    g=valor/0.5;
    b=1;
    cor=[r g b];
    
    return;
end

r=1;
g=1-((valor-0.5)/0.5);
b=0;

cor=[r g b];
end

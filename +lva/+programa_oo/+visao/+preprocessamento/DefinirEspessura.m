classdef DefinirEspessura<gui.containers.Janela
    
    properties(Access='private')
        
        rotuloEspessura;
        rotuloComprimento;
        cxTxtEspessura;
        cxTxtComprimento;
        btConfirmar;
        btCancelar;
        dados;
        
    end
    
    
    methods
        
        function obj=DefinirEspessura()
            obj@gui.containers.Janela('Definir espessura');
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.dimensionarComponentes();
            obj.fixarDimensao(175,75);
            obj.adicionarAcoesDosComponentes();
             set(obj.obterId(),'WindowStyle','modal');
             obj.fixarVisivel(true);
            obj.dados.espessura=0;
            obj.dados.comprimento=0;
             uiwait(obj.obterId());
        end
        
        function criarComponentes(obj)
            
            import gui.componentes.CaixaDeTexto;
            import gui.componentes.Botao;
            import gui.componentes.Rotulo;
            
            obj.rotuloEspessura=Rotulo(obj);
            obj.rotuloComprimento=Rotulo(obj);
            obj.cxTxtEspessura=CaixaDeTexto(obj);
            obj.cxTxtComprimento=CaixaDeTexto(obj);
            obj.btConfirmar=Botao(obj);
            obj.btCancelar=Botao(obj);
            
            obj.rotuloEspessura.fixarTexto('espessura');
            obj.rotuloComprimento.fixarTexto('comprimento');
            obj.cxTxtEspessura.fixarTexto('0');
            obj.cxTxtComprimento.fixarTexto('0');
            obj.btConfirmar.fixarTexto('confirmar');
            obj.btCancelar.fixarTexto('cancelar');
            
            
        end
        
        function posicionarComponentes(obj)
            
            obj.rotuloEspessura.fixarDimensao(80,18);
            obj.rotuloComprimento.fixarDimensao(80,18);
            obj.cxTxtEspessura.fixarDimensao(80,20);
            obj.cxTxtComprimento.fixarDimensao(80,20);
            obj.btConfirmar.fixarDimensao(80,20);
            obj.btCancelar.fixarDimensao(80,20);
            
        end
        
        
        function dimensionarComponentes(obj)
            
            obj.rotuloEspessura.fixarPosicao(90,60);
            obj.rotuloComprimento.fixarPosicao(5,60);
            obj.cxTxtEspessura.fixarPosicao(90,30);
            obj.cxTxtComprimento.fixarPosicao(5,30);
            obj.btConfirmar.fixarPosicao(5,5);
            obj.btCancelar.fixarPosicao(90,5);
        end
        
        
        function adicionarAcoesDosComponentes(obj)
            obj.btConfirmar.addlistener('componenteClicado',@obj.acaoBotaoConfirmar);
            obj.btCancelar.addlistener('componenteClicado',@obj.acaoBotaoCancelar);
        end
        
        function acaoBotaoCancelar(obj,src,evt)
            close(obj.obterId());
        end
        
        function acaoBotaoConfirmar(obj,src,evt)
            obj.dados.espessura=str2double(obj.cxTxtEspessura.obterTexto());
            obj.dados.comprimento=str2double(obj.cxTxtComprimento.obterTexto());
            close(obj.obterId());
        end
        
        function d=obterDados(obj)
            d=obj.dados;
        end
    end
    
end


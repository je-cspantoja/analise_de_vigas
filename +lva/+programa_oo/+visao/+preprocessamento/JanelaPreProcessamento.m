classdef JanelaPreProcessamento<gui.containers.Janela
    
    
    properties(Access='private')
        
        
        painelFaceDireita;
        painelFaceEsquerda;
        
        cxTextoComprimento;
        cxTextoNumeroDeElementos;
        btVisualizar;
        painel;
        rotuloComprimento;
        rotuloNumElementos;
        
       
        cxListagemEspessuras;
        cxListagemAlturas;
        saidaDeDados;
        painelFerramentas;
        quadro3D;
        
        btAdicionarEsp;
        btAdicionarAl;
        
        btRemoverEsp;
        btRemoverAl;
        preProcessamento;
    end
    
    methods
        function obj=JanelaPreProcessamento()
            obj@gui.containers.Janela('Pre-Processamento');
            obj.fixarPosicao(1,30);
            tamanhoTela=get(0,'ScreenSize');
            obj.habilitarBarraDeFerramentasDeFigura(true);
            obj.fixarDimensao(tamanhoTela(3),tamanhoTela(4)-80);
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.adicionarAcoesAosComponentes();
            obj.fixarVisivel(true);
            import lva.programa_oo.modelo.fachada.PreProcessamento;
            obj.preProcessamento=PreProcessamento();
            
        end
        
    end
    
    methods(Access='private')
        
        
        function criarComponentes(obj)
            
            import gui.componentes.CaixaDeTexto;
            import gui.componentes.Botao;
            import gui.containers.Painel;
            import gui.componentes.CaixaDeListagem;
            import lva.programa_oo.visao.preprocessamento.PainelProPriedadesDaFace;
            import gui.componentes.Rotulo;
            import lva.programa_oo.visao.PainelSaidaDeDados;
            import lva.programa_oo.visao.Quadro3D; 
            
            obj.saidaDeDados=PainelSaidaDeDados(obj,'Arquivo de saida pre-processamento');
            obj.quadro3D=Quadro3D(obj);
            obj.quadro3D.eVisivel(false);
            obj.painelFerramentas=Painel(obj,'');
            obj.cxTextoNumeroDeElementos=CaixaDeTexto(obj.painelFerramentas);
            obj.cxTextoComprimento=CaixaDeTexto(obj.painelFerramentas);
            obj.painelFaceDireita=PainelProPriedadesDaFace(obj.painelFerramentas,'Face Direita');
            obj.painelFaceEsquerda=PainelProPriedadesDaFace(obj.painelFerramentas,'Face Esquerda');
            obj.cxListagemEspessuras=CaixaDeListagem(obj.painelFerramentas);
            obj.cxListagemAlturas=CaixaDeListagem(obj.painelFerramentas);
            obj.btVisualizar=Botao(obj.painelFerramentas);
            obj.rotuloComprimento=Rotulo(obj.painelFerramentas);
            obj.rotuloNumElementos=Rotulo(obj.painelFerramentas);
            
            obj.btAdicionarEsp=Botao(obj.painelFerramentas);
            obj.btAdicionarAl=Botao(obj.painelFerramentas);
            obj.btRemoverEsp=Botao(obj.painelFerramentas);
            obj.btRemoverAl=Botao(obj.painelFerramentas);
            
            obj.cxTextoNumeroDeElementos.fixarTexto('100');
            obj.cxTextoComprimento.fixarTexto('100');
            obj.btVisualizar.fixarTexto('Gerar modelo');
            obj.rotuloComprimento.fixarTexto('Comprimento');
            obj.rotuloNumElementos.fixarTexto('Nº elementos');
            

            obj.rotuloNumElementos.fixarAlinhamentoHorizontal(Rotulo.esquerda);
            obj.rotuloComprimento.fixarAlinhamentoHorizontal(Rotulo.esquerda);
            
            obj.saidaDeDados.fixarDimensao(660,50);
            obj.painelFerramentas.fixarDimensao(660,110);
            obj.cxTextoComprimento.fixarDimensao(80,20);
            obj.cxTextoNumeroDeElementos.fixarDimensao(80,20);
            obj.btVisualizar.fixarDimensao(120,20);
            obj.rotuloComprimento.fixarDimensao(100,18);
            obj.rotuloNumElementos.fixarDimensao(100,18);
            obj.cxListagemEspessuras.fixarDimensao(100,70);
            obj.cxListagemAlturas.fixarDimensao(100,70);
            [x y]=obj.obterDimensao();
            obj.quadro3D.fixarDimensao(x-5,y-170);
            
            
            obj.btAdicionarEsp.fixarDimensao(20,20);
            obj.btAdicionarAl.fixarDimensao(20,20);
            obj.btRemoverEsp.fixarDimensao(20,20);
            obj.btRemoverAl.fixarDimensao(20,20);
           
            
            obj.btAdicionarEsp.fixarTexto('+');
            obj.btAdicionarAl.fixarTexto('+');
            obj.btRemoverEsp.fixarTexto('-');
            obj.btRemoverAl.fixarTexto('-');
            

% experimental
             obj.cxListagemEspessuras.eVisivel(false);
            obj.cxListagemAlturas.eVisivel(false);
               
            obj.btAdicionarEsp.eVisivel(false);
            obj.btAdicionarAl.eVisivel(false);
            obj.btRemoverEsp.eVisivel(false);
            obj.btRemoverAl.eVisivel(false);
%          
        end
        
        
        function fixarArquivoDeSaida(obj,src,evt)
            
        obj.preProcessamento.fixarArquivoSaida(src.obterNomeDoArquivo());
        
        end
        
        function adicionarEspessura(obj,src,evt)
        cxDialogo=lva.programa_oo.visao.preprocessamento.DefinirEspessura();
        d=cxDialogo.obterDados();
        obj.preProcessamento.adicionarEspessuraDadoComprimento(d.comprimento,d.espessura);
        s=sprintf('(%d,%d)',d.comprimento,d.espessura);
        
        itens=obj.cxListagemEspessuras.obterTexto();
        itens=[itens; {s}];
        obj.cxListagemEspessuras.fixarTexto(itens);
        
        end
        
        function adicionarAltura(obj,src,evt)
            cxDialogo=lva.programa_oo.visao.preprocessamento.DefinirAltura();
            d=cxDialogo.obterDados();
            
            obj.preProcessamento.adicionarAlturaDadoComprimento(d.comprimento,d.altura);
            s=sprintf('(%d,%d)',d.comprimento,d.altura);
            
            itens=obj.cxListagemAlturas.obterTexto();
            itens=[itens; {s}];
            obj.cxListagemAlturas.fixarTexto(itens);
            
        end

        
        function removerEspessura(obj,src,evt)
            obj.cxListagemEspessuras.removerItemSelecionado();
            
        end
        
        function removerAltura(obj,src,evt)
           obj.cxListagemAlturas.removerItemSelecionado();
           
        end
        
        
        function posicionarComponentes(obj)
            [x,y]=obj.obterDimensao();
            
            obj.saidaDeDados.fixarPosicao(5,y-50);
            obj.quadro3D.fixarPosicao(5,135);
            obj.painelFerramentas.fixarPosicao(5,5);
            
            
            obj.painelFaceEsquerda.fixarPosicao(110,5);
            obj.painelFaceDireita.fixarPosicao(215,5);
            
            obj.cxListagemEspessuras.fixarPosicao(320,35);
            obj.cxListagemAlturas.fixarPosicao(425,35);
            
            obj.cxTextoComprimento.fixarPosicao(5,55);
            obj.rotuloComprimento.fixarPosicao(5,80);
            
            obj.cxTextoNumeroDeElementos.fixarPosicao(5,5);
            obj.rotuloNumElementos.fixarPosicao(5,30);
            
            obj.btVisualizar.fixarPosicao(530,5);
           
            
            obj.btAdicionarEsp.fixarPosicao(320,5);
            obj.btRemoverEsp.fixarPosicao(345,5);
            
            obj.btAdicionarAl.fixarPosicao(425,5);
            
            obj.btRemoverAl.fixarPosicao(450,5);
           
            
        end
        
        function adicionarAcoesAosComponentes(obj)
            obj.btVisualizar.addlistener('componenteClicado',@obj.acaoBotaoVisualizar);
            
            obj.btAdicionarEsp.addlistener('componenteClicado',@obj.adicionarEspessura);
            obj.btAdicionarAl.addlistener('componenteClicado',@obj.adicionarAltura);
            obj.btRemoverEsp.addlistener('componenteClicado',@obj.removerEspessura);
            obj.btRemoverAl.addlistener('componenteClicado',@obj.removerAltura);
            obj.saidaDeDados.addlistener('modificacaoNomeDoArquivo',@obj.fixarArquivoDeSaida);
           
        end
        
        
        function acaoBotaoVisualizar(obj,src,evt)
            
            
            try
                
                strComprimento=obj.cxTextoComprimento.obterTexto();
                strNumElementos=obj.cxTextoNumeroDeElementos.obterTexto();
                
                comprimento=str2double(strComprimento);
                numElementos=str2double(strNumElementos);
                
                faceEsq=obj.painelFaceEsquerda.obterDadosDaFace();
                faceDir=obj.painelFaceDireita.obterDadosDaFace();
                

               
            catch e
                
                javax.swing.JOptionPane.showMessageDialog([],'Erro inesperado, comunique os desenvolvedores.');
               
            end
            
            try
                
                obj.preProcessamento.fixarDimensoesDaFaceEsquerda(faceEsq);
                obj.preProcessamento.fixarDimensoesDaFaceDireita(faceDir);
                obj.preProcessamento.fixarComprimento(comprimento);
                
                geoms=obj.preProcessamento.discretizar(numElementos);
                
                geoms3d=obj.quadro3D.converterEmFiguras3D(geoms);
                obj.quadro3D.desenharFiguras3D(geoms3d);
            catch e
                
                javax.swing.JOptionPane.showMessageDialog([],e.message);
                return;
              
            end
            
                javax.swing.JOptionPane.showMessageDialog([],'Arquivo gerado com sucesso!');
        end
        
       
    end
end


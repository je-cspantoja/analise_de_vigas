classdef DefinirAltura<gui.containers.Janela
    
    properties(Access='private')
        
        rotuloAltura;
        rotuloComprimento;
        cxTxtAltura;
        cxTxtComprimento;
        btConfirmar;
        btCancelar;
        dados;
        
    end
    
    
    methods
        
        function obj=DefinirAltura()
            
            obj@gui.containers.Janela('Adicionar altura');
            obj.criarComponentes();
            obj.posicionarComponentes();
            obj.dimensionarComponentes();
            obj.fixarDimensao(175,75);
            obj.adicionarAcoesDosComponentes();
            obj.dados.altura=0;
            obj.dados.comprimento=0;
             set(obj.obterId(),'WindowStyle','modal');
             uiwait(obj.obterId());
        end
        
        function criarComponentes(obj)
            
            import gui.componentes.CaixaDeTexto;
            import gui.componentes.Botao;
            import gui.componentes.Rotulo;
            
            obj.rotuloAltura=Rotulo(obj);
            obj.rotuloComprimento=Rotulo(obj);
            obj.cxTxtAltura=CaixaDeTexto(obj);
            obj.cxTxtComprimento=CaixaDeTexto(obj);
            obj.btConfirmar=Botao(obj);
            obj.btCancelar=Botao(obj);
            
            obj.rotuloAltura.fixarTexto('altura');
            obj.rotuloComprimento.fixarTexto('comprimento');
            obj.cxTxtAltura.fixarTexto('0');
            obj.cxTxtComprimento.fixarTexto('0');
            obj.btConfirmar.fixarTexto('confirmar');
            obj.btCancelar.fixarTexto('cancelar');
            
            
        end
        
        function posicionarComponentes(obj)
            
            obj.rotuloAltura.fixarDimensao(80,18);
            obj.rotuloComprimento.fixarDimensao(80,18);
            obj.cxTxtAltura.fixarDimensao(80,20);
            obj.cxTxtComprimento.fixarDimensao(80,20);
            obj.btConfirmar.fixarDimensao(80,20);
            obj.btCancelar.fixarDimensao(80,20);
            
        end
        
        
        function dimensionarComponentes(obj)
            
            obj.rotuloAltura.fixarPosicao(90,60);
            obj.rotuloComprimento.fixarPosicao(5,60);
            obj.cxTxtAltura.fixarPosicao(90,30);
            obj.cxTxtComprimento.fixarPosicao(5,30);
            obj.btConfirmar.fixarPosicao(5,5);
            obj.btCancelar.fixarPosicao(90,5);
        end
        
        
        function adicionarAcoesDosComponentes(obj)
            obj.btConfirmar.addlistener('componenteClicado',@obj.acaoBotaoConfirmar);
            obj.btCancelar.addlistener('componenteClicado',@obj.acaoBotaoCancelar);
        end
        
        function acaoBotaoCancelar(obj,src,evt)

            close(obj.obterId());
        end
        
        function acaoBotaoConfirmar(obj,src,evt)
            obj.dados.altura=str2double(obj.cxTxtAltura.obterTexto());
            obj.dados.comprimento=str2double(obj.cxTxtComprimento.obterTexto());
            close(obj.obterId());
        end
        
        function d=obterDados(obj)
            d=obj.dados;
        end
    end
    
end


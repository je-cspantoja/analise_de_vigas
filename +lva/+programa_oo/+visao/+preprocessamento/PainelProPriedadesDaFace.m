classdef PainelProPriedadesDaFace<gui.containers.Painel
    
    
    properties(Access='private')
        
        cxTxtAltura;
        cxTxtEspessura;
        rotuloAltura;
        rotuloEspessura;
    end
    
    methods
        
        function obj=PainelProPriedadesDaFace(pai,titulo)
            
            obj@gui.containers.Painel(pai,titulo);
            obj.criarComponentes();
            obj.dimensionarComponentes();
            obj.posicionarComponentes();
            obj.fixarDimensao(100,100);
            
        end
        
        function criarComponentes(obj)
            import gui.componentes.CaixaDeTexto;
            import gui.componentes.Botao;
            import gui.componentes.Rotulo
            
            obj.cxTxtAltura=CaixaDeTexto(obj);
            obj.cxTxtEspessura=CaixaDeTexto(obj);
            obj.rotuloAltura=Rotulo(obj);
            obj.rotuloEspessura=Rotulo(obj);
            obj.cxTxtAltura.fixarTexto('10');
            obj.cxTxtEspessura.fixarTexto('10');
            
            obj.rotuloAltura.fixarTexto('altura');
            obj.rotuloEspessura.fixarTexto('espessura');
        end
        
        function dimensionarComponentes(obj)
            obj.cxTxtAltura.fixarDimensao(90,20);
            obj.cxTxtEspessura.fixarDimensao(90,20);
             obj.rotuloAltura.fixarDimensao(90,18);
             obj.rotuloEspessura.fixarDimensao(90,18);
        end
        
        
        function posicionarComponentes(obj)
            
            obj.cxTxtAltura.fixarPosicao(5,45);
            obj.rotuloAltura.fixarPosicao(5,65);
            obj.cxTxtEspessura.fixarPosicao(5,5);
            obj.rotuloEspessura.fixarPosicao(5,25);
            
            
        end
        
        
        function face=obterDadosDaFace(obj)
            try
                
                strAltura=obj.cxTxtAltura.obterTexto();
                strEspessura=obj.cxTxtEspessura.obterTexto();
                face.altura=str2double(strAltura);
                face.espessura=str2double(strEspessura);
                
            catch erro
                face.altura=0;
                face.espessura=0;
                obj.cxTxtAltura.fixarTexto('0');
                obj.cxTxtEspessura.fixarTexto('0');
                
            end
            
        end
    end
    
end


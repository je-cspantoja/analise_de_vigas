classdef PainelEntradaDeDados<lva.programa_oo.visao.PainelArquivo
  
    
      
    methods
        
        function obj=PainelEntradaDeDados(pai,titulo)
            obj@lva.programa_oo.visao.PainelArquivo(pai,titulo);
            obj.fixarDimensao(300,50);
            
        end
        
         
        
    end
    methods(Access='protected')
        function nomeCompleto=mostrarCaixaDeDialogo(obj)
            [filename, pathname] = uigetfile('*.mat', '');
            nomeCompleto=[pathname filename];
            
        end
    end
end